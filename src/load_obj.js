import {Color, Point2D, Point3D, Transform} from "./math.js";
import {Material, Model, Vertex, DrawMode, CachedPointArray} from "./drawable.js";
import {downloader, Downloader} from "./downloader.js";


export class ObjLoader extends Model
{
    constructor(scene, relative_uris=true, callback_loaded=null)
    {
        super([], null, new Transform());
        this.scene = scene;
        this.objects = [];
        this.vertices = [];
        this.uv = [];
        this.normals = [];
        this.materials = {};
        this.current_groups = ["default"];
        this.current_object = null;
        this.current_material = null;
        if ( !this.scene )
            this.textures_to_load = new Set();
        this.prefix = "";
        this.relative_uris = relative_uris;
        this.callback_loaded = callback_loaded;
        this.downloader_id = downloader.new_sender_id();
        this.downloader_group = `obj_${this.downloader_id}`;
    }

    load_obj(uri)
    {
        if ( this.relative_uris && !this.prefix )
            this.prefix = /(.*\/)?([^/]+)/.exec(uri)[1] || "";
        else
            uri = this.prefix + uri;

        downloader.download(
            uri,
            this.on_load_obj.bind(this),
            {},
            this.downloader_group
        );
    }

    load_mtl(uri)
    {
        downloader.download(
            this.prefix + uri,
            this.on_load_mtl.bind(this),
            {},
            this.downloader_group
        );
    }

    load_textures(scene)
    {
        if ( this.scene )
            return;

        for ( var t of this.textures_to_load )
            this.scene.load_texture(t, t);
        this.textures_to_load = new Set();
    }

    on_render(scene, renderer)
    {
        this.load_textures(scene);
        super.on_render(scene, renderer);
    }

    on_load_finished(job)
    {
        if ( job.downloader.group_is_completed(
            this.downloader_group,
            Downloader.Status.LOAD_START
        ) )
        {
            job.downloader.clear_group(job.group);
            var default_material = new Material();
            for ( var obj of this.objects )
            {
                if ( obj.loaded )
                    continue;
                obj.loaded = true;
                var model = new Model([]);
                for ( var group of obj.groups )
                {
                    var material = group.material ? this.materials[group.material] : null;
                    if ( !material )
                        material = default_material;
                    /*
                    // This version pushes each face individually
                    // which is better for polygon sorting but a bit slow to render
                    var grp_mesh = new Model([], material, null);
                    for ( var elem of group.elements )
                    {
                        elem.push_into(grp_mesh);
                    }
                    model.push(grp_mesh);*/
                    var primitive = new CachedPointArray([], material);
                    var i = 0;
                    for ( var elem of group.elements )
                    {
                        if ( elem.mode != primitive.mode )
                        {
                            if ( primitive.points.length )
                            {
                                model.push(primitive);
                            }
                            primitive = new CachedPointArray([], material, elem.mode);
                        }
                        elem.push_into(primitive);
                    }
                    model.push(primitive);
                }
                this.push(model);
                if ( obj.name )
                    model.name = obj.name;
            }
            if ( this.callback_loaded )
                this.callback_loaded(this);
        }
    }

    on_load_obj(job)
    {
        this.push_object();
        this.current_groups = ["default"];
        this.on_load(job.response);
        this.push_object();
        this.on_load_finished(job);
    }

    on_load_mtl(job)
    {
        this.on_load(job.response);
        this.on_load_finished(job);
    }

    push_object()
    {
        if ( this.current_object )
            this.push_group();
        if ( this.current_object && this.current_object.groups.length )
        {
            this.objects.push(this.current_object);
        }
        this.current_object = {
            groups: [],
            current_group: null,
            name: "",
        }
        this.push_group();
    }

    push_group()
    {
        if ( this.current_object.current_group &&
             this.current_object.current_group.elements.length )
        {
            this.current_object.groups.push(this.current_object.current_group);
        }
        this.current_object.current_group = {
            smoothing_group: 0,
            elements: [],
            loaded: false,
        };
    }

    maybe_push_group()
    {
        if ( !this.current_object.current_group || this.current_object.current_group.elements.length )
            this.push_group();
    }

    on_load(text)
    {
        var spaces = /\s+/;
        for ( var line of text.split("\n") )
        {
            line = line.trim();
            var args = line.split(spaces);
            if ( args.length == 0 || line == "" || args[0][0] == "#" )
                continue;

            var cmd = args[0];
            args = args.slice(1);

            var method = this["on_load_cmd_" + cmd];
            if ( !method )
                console.log(`Unimplemented ${cmd}`);
            else
                method.bind(this)(args);
        }
    }

    on_load_cmd_mtllib(args)
    {
        for ( var mtl of args )
            this.load_mtl(mtl);
    }

    on_load_cmd_o(args)
    {
        if ( args.length == 0 )
            return;

        this.push_object();
        this.current_object.name = args[0];
    }

    on_load_cmd_g(args)
    {
        // Note: does nothing
        if ( args.length == 0 )
            this.current_groups = ["default"];
        else
            this.current_groups = args;
    }

    on_load_cmd_v(args)
    {
        // Note: ignores w
        if ( args.length < 3 )
        {
            console.log("Invalid geometric vertex");
            return;
        }

        this.vertices.push(new Point3D(...args.slice(0, 3).map(Number)));
    }

    on_load_cmd_vt(args)
    {
        // Note: ignores w and requires 2D textures
        if ( args.length < 2 )
        {
            console.log("Invalid texture vertex");
            return;
        }

        this.uv.push(new Point2D(...args.slice(0, 2).map(Number)));
    }

    on_load_cmd_vn(args)
    {
        if ( args.length != 3 )
        {
            console.log("Invalid normal vector");
            return;
        }

        this.normals.push(new Point3D(...args.map(Number)));
    }

    on_load_cmd_usemtl(args)
    {
        if ( args.length != 1 )
        {
            console.log("Must specify exactly 1 material name for usemtl");
            return;
        }

        this.maybe_push_group();
        this.current_object.current_group.material = args[0];
    }

    on_load_cmd_s(args)
    {
        // Note: does nothing
        if ( args.length != 1 )
        {
            console.log("Must specify exactly 1 smoothing group number for s");
            return;
        }

        var number = args[0] == "off" ? 0 : Number(args[0]);

        this.maybe_push_group();
        this.smoothing_group = number;
    }

    on_load_cmd_f(args)
    {
        if ( args.length < 3 )
        {
            console.log("Not enough vertices to form a face");
            return;
        }
        var face = new ObjElement(ObjElement.Type.FACE);
        for ( var vertex of args )
        {
            var indices = vertex.split("/");
            if ( indices.length < 1 || indices > 3 )
            {
                console.log("Invalid vertex specification");
                return;
            }
            var point = new Vertex(this.get_point(this.vertices, indices[0]));
            if ( indices[1] )
                point.uv = this.get_point(this.uv, indices[1]);
            if ( indices[2] )
                point.normal = this.get_point(this.normals, indices[2]);
            face.vertices.push(point);
        }

        this.current_object.current_group.elements.push(face);
    }

    get_point(array, index)
    {
        var i = Number(index);
        if ( index < 0 )
            i = array.length + index;
        else if ( index > 0 )
            i -= 1;
        else
            i = -1;

        if ( i < 0 || i >= array.length )
        {
            console.log("Invalid vertex index");
            return;
        }
        return array[i];
    }

    // mtl loading: http://paulbourke.net/dataformats/mtl/
    // TODO Tf sharpness map_d map_aat decal disp bump refl

    on_load_cmd_newmtl(args)
    {
        if ( args.length != 1 )
        {
            console.log("Must specify exactly 1 material name");
            return;
        }
        this.current_material = new Material();
        this.current_material.name = args[0];
        this.materials[this.current_material.name] = this.current_material;
    }

    on_load_color(color_type, args)
    {
        if ( args.length == 0 )
        {
            console.log("Invalid color");
            return;
        }

        if ( args[1] == "xyz" )
        {
            console.log("xyz not implemented");
        }
        else if ( args[1] == "spectral" )
        {
            console.log("spectral not implemented");
        }
        else
        {
            var r = Number(args[0]);
            this.current_material[color_type] = new Color(
                r,
                args.length > 1 ? Number(args[1]) : r,
                args.length > 2 ? Number(args[2]) : r,
            );
        }
    }

    on_load_cmd_Ka(args)
    {
        this.on_load_color("ambient", args);
    }

    on_load_cmd_Kd(args)
    {
        this.on_load_color("diffuse", args);
    }

    on_load_cmd_Ks(args)
    {
        this.on_load_color("specular", args);
    }

    on_load_cmd_Ke(args)
    {
        this.on_load_color("emissive", args);
    }

    on_load_cmd_illum(args)
    {
        if ( args.length != 1 )
        {
            console.log("Invalid illum");
            return;
        }
        // TODO: do anything about this?
    }

    on_load_cmd_d(args)
    {
        if ( args.length < 1 )
        {
            console.log("Invalid dissolve");
            return;
        }
        if ( args[0] == "-halo" )
        {
            console.log("halo not implemented");
            return;
        }
        this.current_material.opacity = Number(args[0]);
    }

    on_load_cmd_Ns(args)
    {
        if ( args.length != 1 )
        {
            console.log("Invalid exponent");
            return;
        }

        this.current_material.specular_exponent = Number(args[0]);
    }

    on_load_texture(color_type, args)
    {
        if ( args.length == 0 )
        {
            console.log("Invalid texture");
            return;
        }

        // TODO: options
        var texture = this.prefix + args[args.length-1];
        this.current_material[color_type+"_texture"] = texture;

        if ( this.scene )
            this.scene.load_texture(texture, texture);
        else
            this.textures_to_load.add(texture);
    }

    on_load_cmd_map_Ka(args)
    {
        this.on_load_texture("ambient", args);
    }

    on_load_cmd_map_Kd(args)
    {
        this.on_load_texture("diffuse", args);
    }

    on_load_cmd_map_Ks(args)
    {
        this.on_load_texture("specular", args);
    }

    on_load_cmd_map_Ke(args)
    {
        this.on_load_texture("emissive", args);
    }

    on_load_cmd_Ni(args)
    {
        if ( args.length == 0 )
        {
            console.log("Invalid index of refraction");
            return;
        }
        this.current_material.index_of_refraction = Number(args[0]);
    }
}

class ObjElement
{
    constructor(mode, vertices=[])
    {
        this.mode = mode;
        this.vertices = vertices;
    }

    push_into(mesh)
    {
        if ( this.mode == ObjElement.Type.FACE )
            mesh.push_face(this.vertices);
    }
}

ObjElement.Type = Object.freeze({
    FACE: DrawMode.TRIANGLES
});
