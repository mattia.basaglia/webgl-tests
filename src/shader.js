import {downloader, Downloader} from "./downloader.js";
import {Transform, Vector} from "./math.js";

class ShaderAttribute
{
    constructor(gl, program, name, type_name, size)
    {
        this.gl = gl;
        this.attribute = gl.getAttribLocation(program, name);
        if ( this.attribute == -1 )
            throw `Invalid attribute ${name}`;
        this.type_name = type_name;
    }

    set_buffer(buffer, n_components)
    {
        const type = this.gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);
        this.gl.vertexAttribPointer(
            this.attribute,
            n_components,
            type,
            normalize,
            stride,
            offset
        );
        this.gl.enableVertexAttribArray(this.attribute);
    }
}

class ShaderUniform
{
    constructor(gl, program, name, type_name, size=1)
    {
        this.gl = gl;
        this.uniform = gl.getUniformLocation(program, name);
        this.type_name = type_name;
        this.size = size;
        /*if ( this.size > 1 )
        {
            for ( var i = 0; i < this.size; i++ )
                this[i] = new ShaderUniform(gl, program, `${name}[${i}]`, type_name, 1);
        }*/
    }

    set(value)
    {
        var scalar, mat;
        [scalar, mat] = this.type_name.toLowerCase().split("_");
        var suf = scalar[0];
        if ( scalar == "bool" )
            suf = "i";

        if ( mat === undefined && this.size > 1 )
        {
            this.gl[`uniform1${suf}v`](this.uniform, value, 0, Math.min(this.size, value.length));
        }
        else if ( mat === undefined )
        {
            this.gl[`uniform1${suf}`](this.uniform, value);
        }
        else if ( mat.startsWith("mat") )
        {
            var extent = mat[mat.length-1];
            if ( value instanceof Transform )
                value = value.transform;
            this.gl[`uniformMatrix${extent}${suf}v`](this.uniform, false, value);
        }
        else if ( mat.startsWith("vec") )
        {
            var extent = mat[mat.length-1];
            if ( value instanceof Vector )
                value = value.components;
            this.gl[`uniform${extent}${suf}v`](this.uniform, value.slice(0, extent));
        }
    }

    /**
     * \example this.raw_set("uniform4fv", [1,2,3,4]);
     */
    raw_set(gl_setter, ...args)
    {
        this.gl[gl_setter](this.uniform, ...args);
    }
}

const __dir__ = import.meta.url.replace(/[^/]+$/, "");
export const shader_path = __dir__ + "/shaders/";

export class Shader
{
    constructor(gl)
    {
        this.gl = gl;
        this.program = null;
        this.macros = {};
        this.extensions = {};
        this.common_string = "";
        this.ready = false;
        this._sender_id = downloader.new_sender_id();

        if ( this.gl.version == 1 )
        {
            this.load_extension("OES_standard_derivatives", null, "BUMP_ENABLED", "Bump mapping not available");
            var db = this.load_extension("WEBGL_draw_buffers", "GL_EXT_draw_buffers", null, "Draw buffers not available");
            this.gl.drawBuffers = (...args) => db.drawBuffersWEBGL(...args);
        }
    }

    static from_uris(
        gl,
        vertex="vertex.glsl",
        fragment="fragment.glsl",
        common="common.glsl",
        dir=shader_path+"main/",
    )
    {
        var shader = new Shader(gl);
        downloader.download(
            dir+common,
            shader._downloaded_common.bind(shader),
            {},
            `shader_common${shader._sender_id}`
        );
        downloader.download(
            dir+vertex,
            shader._downloaded_src.bind(shader),
            {type: shader.gl.VERTEX_SHADER},
            `shader_${shader._sender_id}`
        );
        downloader.download(
            dir+fragment,
            shader._downloaded_src.bind(shader),
            {type: shader.gl.FRAGMENT_SHADER},
            `shader_${shader._sender_id}`
        );
        return shader;
    }

    _downloaded_common(job)
    {
        this.common_string += job.response;

        if ( job.downloader.group_is_completed(job.group, Downloader.Status.LOAD_START) )
        {
            job.downloader.clear_group(job.group);

            this.common_string = this._get_shader_macros_src() + this.common_string + "#line 1\n";

            this.program = this.gl.createProgram();
            for ( var ojob of job.downloader.groups[`shader_${this._sender_id}`] )
            {
                if ( ojob.status > Downloader.Status.LOAD_START )
                    this._downloaded_compile(ojob);
            }
        }
    }

    _downloaded_src(job)
    {
        job.status = Downloader.Status.CUSTOM;
        if ( this.program )
            this._downloaded_compile(job);
    }

    _downloaded_compile(job)
    {
        var type = job.meta.type;

        var header = "";
        if ( this.gl.version == 2 )
        {
            header += "#version 300 es\n";
        }

        if ( type == this.gl.VERTEX_SHADER )
        {
            header += "#define SHADER_VERTEX\n";
            type = this.gl.VERTEX_SHADER;
        }
        else if ( type == this.gl.FRAGMENT_SHADER )
        {
            header += "#define SHADER_FRAGMENT\n";
        }

        this.compile(type, header + this.common_string + job.response, job.uri);

        job.status = Downloader.Status.LOAD_END;


        if ( job.downloader.group_is_completed(job.group, Downloader.Status.LOAD_END) )
        {
            job.downloader.clear_group(job.group);
            this.link();
        }
    }

    link()
    {
        this.gl.linkProgram(this.program);

        if ( !this.gl.getProgramParameter(this.program, this.gl.LINK_STATUS) )
            throw "Unable to initialize the shader program: " + this.gl.getProgramInfoLog(this.program);

        this.load_uniform_atrributes();
        this.ready = true;
    }

    compile(type, sources, uri)
    {

        const shader = this.gl.createShader(type);

        this.gl.shaderSource(shader, sources);

        this.gl.compileShader(shader);

        if ( !this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS) )
        {
            var info = this.gl.getShaderInfoLog(shader);
            this.gl.deleteShader(shader);
            throw `Error compiling ${uri}: ${info}`;
        }

        this.gl.attachShader(this.program, shader);

        return shader;
    }


    load_shader(uri, type)
    {
        var src = new ShaderSource(uri, type, this.on_loaded_shader.bind(this));
        if ( type == ShaderSource.Type.COMMON )
            this.common_sources.push(src);
        else
            this.sources.push(src);
    }

    load_extension(name, glsl_ext_name=null, macro=null, message="Failed to load extension")
    {
        this.extensions[name] = this.gl.getExtension(name);
        if ( !this.extensions[name] )
        {
            console.warn(`${message} (${name})`);
        }
        else
        {
            this.extensions[name].name = glsl_ext_name ? glsl_ext_name : `GL_${name}`;
            this.macros[macro ? macro : name] = 1;
        }
        return this.extensions[name];
    }

    _get_shader_macros_src()
    {
        var txt = "";
        for ( var mn in this.macros )
            txt += `#define ${mn} ${this.macros[mn]}\n`;

        for ( var ex in this.extensions )
        {
            if ( this.extensions[ex] )
                txt += `#extension ${this.extensions[ex].name} : enable\n`;
        }

        if ( txt.length )
            txt += `#line 1\n`;
        return txt;
    }

    load_uniform_atrributes()
    {
        var types = [
            "FLOAT",
            "FLOAT_MAT2", "FLOAT_MAT3", "FLOAT_MAT4",
            "FLOAT_VEC2", "FLOAT_VEC3", "FLOAT_VEC4",
            "INT",
            "INT_VEC2", "INT_VEC3", "INT_VEC4",
            "BOOL",
            "BOOL_VEC2", "BOOL_VEC3", "BOOL_VEC4",
            "SAMPLER_2D", "SAMPLER_CUBE",
            "UNSIGNED_INT",
        ];
        var types_lookup = {};
        types.forEach((t => types_lookup[this.gl[t]] = t).bind(this));

        this.uniform = this._load_vars(
            this.gl.ACTIVE_UNIFORMS,
            this.gl.getActiveUniform,
            ShaderUniform,
            types_lookup
        )
        this.attribute = this._load_vars(
            this.gl.ACTIVE_ATTRIBUTES,
            this.gl.getActiveAttrib,
            ShaderAttribute,
            types_lookup
        )
    }

    // Initialize a shader program, so WebGL knows how to draw our data
    init_shaders()
    {
        // Create the shader program

        const shader_program = this.gl.createProgram();
        this.gl.attachShader(shader_program, vertex_shader);
        this.gl.attachShader(shader_program, fragment_shader);
        this.gl.linkProgram(shader_program);

        // If creating the shader program failed, alert

        if (!this.gl.getProgramParameter(shader_program, this.gl.LINK_STATUS))
            throw "Unable to initialize the shader program: " + this.gl.getProgramInfoLog(shader_program);

        return shader_program;
    }

    load_attribute(name)
    {
        this.attribute[name] = new ShaderAttribute(this.gl, this.program, name);
    }

    _load_vars(param, access, ctor, types_lookup)
    {
        var dict = {};
        access = access.bind(this.gl);
        var count = this.gl.getProgramParameter(this.program, param);
        for ( var i = 0; i < count; i++ )
        {
            var active_info = access(this.program, i);
            var type = types_lookup[active_info.type];
            var name_tokens = active_info.name.split(/[[.\]]/).filter(x => x);
            var last = name_tokens.pop();
            var pre_last;
            if ( active_info.size > 1 )
                pre_last = name_tokens.pop();
            var obj = dict;
            for ( var tok of name_tokens )
            {
                if ( tok.length )
                {
                    if ( obj[tok] === undefined )
                        obj[tok] = {};

                    obj = obj[tok];
                }
            }

            if ( active_info.size > 1 )
                last = pre_last;

            obj[last] = new ctor(this.gl, this.program, active_info.name, type, active_info.size);
        }
        return dict;
    }
}
