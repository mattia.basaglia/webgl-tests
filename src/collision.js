import {Point3D} from "./math.js";

export class Ray
{
    constructor(start, end)
    {
        this._start = start;
        this._end = end;
        this._recalculate();
    }

    _recalculate()
    {
        this._vector_length = this._end.clone().subtract(this._start);
        this._direction = this._vector_length.clone().normalize();
    }

    get start()
    {
        return this._start;
    }

    set start(point)
    {
        this._start = point;
        this._recalculate();
    }

    get end()
    {
        return this._end;
    }

    set end(point)
    {
        this._end = point;
        this._recalculate();
    }

    get vector_length()
    {
        return this._vector_length;
    }

    get direction()
    {
        return this._direction;
    }

    /**
     * \brief Point between start (ratio=0) and end (ratio=1)
     */
    point_at(ratio)
    {
        if ( ratio === null )
            return null;
        return this._vector_length.clone().multiply(ratio).add(this._start);
    }

    /**
     * \brief Point on the ray the given lengh away from the start
     */
    point_at_length(length)
    {
        return this._direction.clone().multiply(length).add(this._start);
    }

    /**
     * \brief Returns a point intersection between a ray and an infinite plane
     * \param ray array of two 3D points
     * \param plane_point a point on the plane
     * \param plane_normal normal of the plane
    */
    ratio_intersect_plane(plane_point, plane_normal)
    {
        // vector from ray start to plane_point
        var ray2plane = plane_point.clone().subtract(this._start);

        // project the ray direction to the normal
        var vp = this._vector_length.dot(plane_normal);
        // project ray2plane to the normal
        var wp = ray2plane.dot(plane_normal);
        var t = wp / vp;

        if ( t === Infinity )
            return null;

        return t;
    }

    intersect_plane(plane_point, plane_normal)
    {
        return this.point_at(this.ratio_intersect_plane(plane_point, plane_normal));
    }

    ratio_intersect_sphere(center, radius)
    {
        var ray2center = center.clone().subtract(this._start);

        // Project the sphere center to the ray (closest point in the ray to the center)
        var center_ray_proj = ray2center.dot(this._direction);

        // Use pythagoras ( v.v == ||v||^2 )
        var rad2 = radius * radius;
        var len_sq = ray2center.dot(ray2center) - (center_ray_proj * center_ray_proj);
        if ( len_sq > rad2 )
            return null;

        // Find the points from the closest point to the edge of the sphere
        var locr = Math.sqrt(rad2 - len_sq);

        // if ray starts inside the sphere, return the far point
        var len = center_ray_proj - locr;
        if ( ray2center.length_squared() < rad2 )
            len = center_ray_proj + locr;

        // return the near point
        return len / this._vector_length.length();
    }

    intersect_sphere(center, radius)
    {
        return this.point_at(this.ratio_intersect_sphere(center, radius));
    }

    /**
     * \pre all points are coplanar
     */
    intersect_ngon(points)
    {
        // Any point, pick the first
        var plane_point = points[0].clone();
        // Find the normal
        var v0 = points[1].clone().subtract(plane_point);
        var v1 = points[2].clone().subtract(plane_point);
        var plane_normal = v0.cross_copy(v1);
        var pos = this.intersect_plane(plane_point, plane_normal);

        // Check if pos is inside the points
        for ( var i = 0; i < points.length; i++ )
        {
            var next = (i + 1) % points.length;
            var edge = points[next].clone().subtract(points[i]);
            var len = pos.clone().subtract(points[i]);
            var cp = edge.cross_copy(len);
            if ( plane_normal.dot(cp) < 0 )
                return null;
        }
        return pos;
    }

    ratio_intersect_aligned_box(b1, b2)
    {
        var collision_min = -Infinity;
        var collision_max = Infinity;

        for ( var c = 0; c < 3; c++ ) // loop xyz
        {
            var t_min = (b1.components[c] - this._start.components[c]) / this._vector_length.components[c];
            var t_max = (b2.components[c] - this._start.components[c]) / this._vector_length.components[c];
            if ( t_min > t_max )
            {
                var t = t_min;
                t_min = t_max;
                t_max = t;
            }
            if ( t_min > collision_min )
                collision_min = t_min;
            if ( t_max < collision_max )
                collision_max = t_max;

            if ( c > 0  && (t_max < collision_min || t_min > collision_max) )
                return null;
        }
        // collision_min < 0 if ray starts inside the box
        return collision_min < 0 ? collision_max : collision_min;
    }

    intersect_aligned_box(b1, b2)
    {
        return this.point_at(this.ratio_intersect_aligned_box(b1, b2));
    }

    ratio_intersect_box(b1, b2, box_transform)
    {
        var t = box_transform.transform;
        var base = [
            new Point3D(t[0], t[1], t[2]),
            new Point3D(t[4], t[5], t[6]),
            new Point3D(t[8], t[9], t[10])
        ];
        var center = b1.clone().add(b2).divide(2);
        var ray_direction = this._end.clone().subtract(this._start);
        var ray2center = center.clone().subtract(this._start);

        var collision_min = -Infinity;
        var collision_max = Infinity;
        // Same as with the aligned box but projecting to the transformed base vectors
        for ( var c = 0; c < 3; c++ ) // loop xyz
        {
            var nom_len = base[c].dot(ray2center);
            var den_len = ray_direction.dot(base[c]);
            var collision_min, collision_max, t_min, t_max;

            t_min = (nom_len + b1.components[c]) / den_len;
            t_max = (nom_len + b2.components[c]) / den_len;
            if ( t_min > t_max )
            {
                var t = t_min;
                t_min = t_max;
                t_max = t;
            }
            if ( t_min > collision_min )
                collision_min = t_min;
            if ( t_max < collision_max )
                collision_max = t_max;

            if ( c > 0  && (t_max < collision_min || t_min > collision_max) )
                return null;
        }

        // collision_min < 0 if ray starts inside the box
        var factor = collision_min < 0 ? collision_max : collision_min;
        return factor;
    }

    intersect_box(b1, b2, box_transform)
    {
        return this.point_at(this.ratio_intersect_box(b1, b2, box_transform));
    }
}
