out lowp vec4 output_color;

uniform mediump float kernel[kernel_sizesq];
uniform mediump float kernel_scale;

uniform sampler2D texture_projector;

lowp vec4 apply_kernel(mediump float kernel[kernel_sizesq], mediump float kernel_scale, lowp vec4 pixels[kernel_sizesq])
{

    lowp vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
    for ( int y = 0; y < kernel_size; y++ )
    {
        for ( int x = 0; x < kernel_size; x++ )
        {
            int id = x+y*kernel_size;
            color += pixels[id] * kernel[id] * kernel_scale;
        }
    }
    return color;
}

lowp vec4 [kernel_size*kernel_size] get_pixels()
{
    ivec2 pixcoord = ivec2(gl_FragCoord.xy);
    ivec2 size = textureSize(texture_projector, 0);
    int off = kernel_size / 2;

    lowp vec4 pixels[kernel_size*kernel_size];
    for ( int y = 0; y < kernel_size; y++ )
    {
        for ( int x = 0; x < kernel_size; x++ )
        {
            ivec2 xy = clamp(pixcoord + ivec2(x-off, y-off), ivec2(0, 0), size);
            pixels[x+y*kernel_size] = texelFetch(texture_projector, xy, 0);
        }
    }
    return pixels;
}


void main()
{
    output_color = apply_kernel(kernel, kernel_scale, get_pixels());
}

