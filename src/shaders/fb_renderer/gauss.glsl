const int max_kernel_size = 32;

out lowp vec4 output_color;

uniform mediump float kernel[max_kernel_size];
uniform ivec2 direction;
uniform int kernel_size;

uniform sampler2D texture_projector;

void main()
{
    ivec2 pixcoord = ivec2(gl_FragCoord.xy);
    ivec2 size = textureSize(texture_projector, 0);

//     output_color = texelFetch(texture_projector, pixcoord, 0);
    output_color = vec4(0.0);
    for ( int i = -kernel_size; i <= kernel_size; i++ )
    {
        ivec2 othcoord = clamp(pixcoord + ivec2(i, i) * direction, ivec2(0, 0), size);
        output_color += texelFetch(texture_projector, othcoord, 0) * kernel[abs(i)];
    }
}
