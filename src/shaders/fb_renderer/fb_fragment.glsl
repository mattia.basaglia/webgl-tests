out lowp vec4 output_color;

uniform sampler2D texture_projector;

void main()
{
    // to get UV divide vec2(gl_FragCoord.xy) by vec2(textureSize(texture_projector, 0))
    ivec2 frag_coord = ivec2(gl_FragCoord.xy);
    output_color = texelFetch(texture_projector, frag_coord, 0);
}
