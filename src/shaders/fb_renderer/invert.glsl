out lowp vec4 output_color;

uniform sampler2D texture_projector;

void main()
{
    output_color = vec4(1.0 - texelFetch(texture_projector, ivec2(gl_FragCoord.xy), 0).rgb, 1.0);
}

