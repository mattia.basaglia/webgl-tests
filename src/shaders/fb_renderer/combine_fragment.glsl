const int max_samplers = 15;
uniform int samplers_used;
struct hax {sampler2D sampler;};
uniform hax sampler[max_samplers];

// #define OUT_COMPOSITE   0
// #define OUT_NORMAL      1
// #define OUT_DIFFUSE     2
// #define OUT_EMISSIVE    3
// #define OUT_FRESNEL     4
// #define OUT_ENVIRONMENT 5

out lowp vec4 output_color;


void main()
{
    ivec2 frag_coord = ivec2(gl_FragCoord.xy);
    for ( int i = 0; i < samplers_used; i++ )
        output_color += texelFetch(sampler[i].sampler, frag_coord, 0);
}
