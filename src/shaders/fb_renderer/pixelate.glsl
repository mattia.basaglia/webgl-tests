out lowp vec4 output_color;

uniform sampler2D texture_projector;

uniform int pixel_size;

void main()
{
    ivec2 frag_coord = ivec2(gl_FragCoord.xy / float(pixel_size)) * pixel_size;
    output_color = texelFetch(texture_projector, frag_coord, 0);
}

