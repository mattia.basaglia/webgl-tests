out lowp vec4 output_color;

uniform sampler2D texture_projector;

uniform highp float z_near;
uniform highp float z_far;

void main()
{
    ivec2 pixcoord = ivec2(gl_FragCoord.xy);
    highp float depth = texelFetch(texture_projector, pixcoord, 0).x;

    // convert z into a linear value (depth buffer is ~ 1/depth)
//     depth = 1.0 - (2.0 * z_near) / (z_far + z_near - depth * (z_far - z_near));

    // depth [0,1] -> [-1, 1]
    depth = 2.0 * depth - 1.0;
    // depth -> world depth [0.1, 100]
    mediump float real_depth = -(2.0 * z_far * z_near) / (depth * (z_far - z_near) - z_near - z_far);
    // normalize -> [0, 1]
    depth = (real_depth - z_near) / (z_far - z_near);
    // invert
    depth = 1.0 - depth;

    output_color = vec4(vec3(depth), 1.0);
}

