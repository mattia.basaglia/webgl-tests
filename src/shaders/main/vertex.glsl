attribute vec3 vertex_position;
attribute vec3 vertex_normal;
attribute vec2 vertex_uv;

uniform mat4 projection_matrix;
uniform mat4 camera_matrix_inv;
uniform mat4 model_view_matrix;
uniform mat4 camera_matrix;
uniform mat4 normal_matrix;


void apply_lights(vec3 vert_pos3)
{

    for ( int i = 0; i < LIGHT_NUMBER; i++ )
    {
        light_info light = lights[i];
        if ( !light.enabled )
            continue;

        v_surface_to_light[i] = normalize(mat3(camera_matrix) * light.direction);

        if ( light.type == 1 )
        {
            vec3 surface_to_light = mat3(camera_matrix) * (v_surface_to_light[i] - vert_pos3);
            v_surface_to_light[i] = normalize(surface_to_light);
        }
    }
}

void apply_reflections()
{
    mat4 world_matrix = camera_matrix * model_view_matrix;
    if ( material.index_of_refraction != 1.0 )
    {
        float ior_intensity = min(1.0, material.index_of_refraction / 10.0);
        vec3 ior_direct = normalize((world_matrix * vec4(vertex_position, 1.0)).xyz);
        vec3 ior_refracted = normalize((world_matrix * vec4(vertex_position + vertex_normal, 1.0)).xyz);
        v_ior_vector = mat3(camera_matrix_inv) * (
            ior_direct * (1.0 - ior_intensity) +
            ior_intensity * ior_refracted
        );
    }

    if ( material.metallic > 0.0 )
    {
        vec3 world_normal = normalize(mat3(normal_matrix) * vertex_normal);
        vec3 cam_vertex = normalize((world_matrix * vec4(vertex_position, 1.0)).xyz);
        v_metallic_vector = mat3(camera_matrix_inv) * reflect(cam_vertex, world_normal);
    }
}

void main()
{
    v_norm = normalize(vec3(normal_matrix * vec4(vertex_normal, 1.0)));
    vec4 vert_pos = camera_matrix * model_view_matrix * vec4(vertex_position, 1.0);
    vec3 vert_pos3 = vec3(vert_pos) / vert_pos.w;
    gl_Position = projection_matrix * vert_pos;
    gl_PointSize = 5.0; // TODO Uniform

    vec4 vpr = camera_matrix * model_view_matrix * vec4(vertex_position, 1.0);
    vec3 vpr3 = vec3(vpr) / vpr.w;
    v_surface_to_camera = normalize(-vpr3);
    v_world_position = vert_pos3;
    if ( material.environment )
    {
        v_ambient_color = vec3(0, 0, 0);
        v_metallic_vector = vertex_position + vertex_normal;
    }
    else
    {
        apply_lights(vert_pos3);
        apply_reflections();
    }

    v_uv = vertex_uv;
}
