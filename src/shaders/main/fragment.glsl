uniform sampler2D sampler_ambient;
uniform sampler2D sampler_diffuse;
uniform sampler2D sampler_specular;
uniform sampler2D sampler_emissive;
uniform sampler2D sampler_displace;
uniform sampler2D sampler_fresnel;

uniform int env_mode;
uniform samplerCube sampler_cube;
uniform sampler2D sampler_hdr;

uniform lowp vec3 light_ambient_color;

precision mediump float;
precision mediump int;

/**
 * \brief Convert a 3D vector to UV coordinates in a HDR image
 */
mediump vec2 texture_hdr(lowp vec3 vec)
{
  mediump float phi = acos(vec.y);
  mediump float theta = atan(-1.0 * vec.x, vec.z) + M_PI;
  return vec2(theta / (2.0 * M_PI), phi / M_PI);
}

/**
 * \brief Given the 3D vector gives the color of the environment map
 */
lowp vec3 environment_color(mediump vec3 position)
{
    if ( env_mode == 1 )
        return textureCube(sampler_cube, position).rgb;
    else if ( env_mode == 2 )
        return texture2D(sampler_hdr, texture_hdr(normalize(position))).rgb;
    else
        return normalize(vec3(abs(position.x), abs(position.y), abs(position.z)));
}

vec2 dHdxy_fwd(vec2 uv)
{
    vec2 dSTdx  = dFdx( uv );
    vec2 dSTdy  = dFdy( uv );
    float Hll   = material.displace_scale * texture2D(sampler_displace, uv).x;
    float dBx   = material.displace_scale * texture2D(sampler_displace, uv + dSTdx).x - Hll;
    float dBy   = material.displace_scale * texture2D(sampler_displace, uv + dSTdy).x - Hll;
    return vec2( dBx, dBy );
}

vec3 perturbNormalArb(vec3 surf_pos, vec3 surf_norm, vec2 v_uv)
{
    // Calc New Fragment Normal based on Bump Map.
    vec2 dHdxy = dHdxy_fwd(v_uv);
    // Workaround for Adreno 3XX dFd*( vec3 ) bug. See #9988
    vec3 vSigmaX = vec3( dFdx( surf_pos.x ), dFdx( surf_pos.y ), dFdx( surf_pos.z ) );
    vec3 vSigmaY = vec3( dFdy( surf_pos.x ), dFdy( surf_pos.y ), dFdy( surf_pos.z ) );
    vec3 vN = surf_norm; // normalized
    vec3 R1 = cross( vSigmaY, vN );
    vec3 R2 = cross( vN, vSigmaX );

    float fDet = dot( vSigmaX, R1 );
    fDet *= ( float( gl_FrontFacing ) * 2.0 - 1.0 );

    vec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );
    return normalize( abs( fDet ) * surf_norm - vGrad );
}

mediump vec3 get_tangent(mediump vec3 normal)
{
    vec3 c1 = cross(normal, vec3(0.0, 0.0, 1.0));
    if ( length(c1) == 0.0 )
        return cross(normal, vec3(0.0, 1.0, 0.0));
    return c1;
}

#define OUT_COMPOSITE   0
#define OUT_NORMAL      1
#define OUT_DIFFUSE     2
#define OUT_EMISSIVE    3
#define OUT_FRESNEL     4
#define OUT_ENVIRONMENT 5


mediump vec3 get_normals()
{
    vec3 bump_normal = normalize(v_norm);
    if ( material.displace_mode == 1 )
    {
        //values between -1 > 1
        bump_normal = perturbNormalArb(v_world_position, bump_normal, v_uv);
    }
    else if ( material.displace_mode == 2 )
    {
        vec3 normal = bump_normal;
        vec3 tangent = (get_tangent(normal));
        vec3 bitangent = (cross(tangent, normal));
        lowp vec3 mapped_normal = normalize((2.0 * texture2D(sampler_displace, v_uv).rgb - 1.0));
        bump_normal = normalize(
            tangent * mapped_normal.x +
            bitangent * mapped_normal.y +
            normal * mapped_normal.z
        );
        bump_normal = material.displace_scale * bump_normal +
                    max(0.0, (1.0 - material.displace_scale)) * normal;
    }
    FragData[OUT_NORMAL] = vec4(bump_normal, 1.0);
    return bump_normal;
}

lowp vec3 mix_colors(lowp vec3 color, lowp vec3 light, bool has_texture, sampler2D sampler)
{
    lowp vec3 c = clamp(color * light, vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0));
    if ( has_texture )
        c *= texture2D(sampler, v_uv).rgb;
    return c;
}

/**
 * \brief Evaluate the color based on phong lighting information
 */
lowp vec3 light_color()
{
    vec3 bump_normal = get_normals();

    lowp vec3 light_diffuse = vec3(0.0, 0.0, 0.0);
    lowp vec3 light_specular = vec3(0.0, 0.0, 0.0);
    lowp float fresnel_val = 0.0;
    lowp float fresnel_count = 0.0;

    for ( int i = 0; i < LIGHT_NUMBER; i++ )
    {
        light_info light = lights[i];
        if ( !light.enabled )
            continue;

        mediump vec3 directional_vector = normalize(v_surface_to_light[i]);
        lowp float brightness = 1.0;

        if ( light.type == 1 )
        {
            mediump float s2llen = length(v_surface_to_light[i]);
            brightness = light.strength / (s2llen * s2llen);
            brightness = clamp(brightness, 0.0, 1.0);
        }

        // Lambert's cosine law
        mediump float lambertian = max(dot(bump_normal, directional_vector), 0.0);
        lowp float specular = 0.0;
        if ( lambertian > 0.0 )
        {
            // Reflected light vector
            vec3 R = reflect(-directional_vector, bump_normal);
            // Compute the specular term
            float specAngle = clamp(dot(R, v_surface_to_camera), 0.0, 1.0);
            specular = pow(specAngle, material.specular_exponent);
        }

        lowp float angle_to_light = dot(bump_normal, directional_vector);

        if ( material.displace_mode != 0 )
        {
            brightness *= angle_to_light;
        }
        else
        {
            brightness *= lambertian;
        }

        light_diffuse += brightness * lambertian * lights[i].color.rgb;
        light_specular += specular * lights[i].color.rgb;

        fresnel_val += angle_to_light / 2.0 + 0.5;
        fresnel_count += 1.0;
    }

    lowp vec3 diffuse = mix_colors(material.ambient.color, light_ambient_color, material.ambient.has_texture, sampler_ambient);
    diffuse += mix_colors(material.diffuse.color, light_diffuse, material.diffuse.has_texture, sampler_diffuse);
    FragData[OUT_DIFFUSE] = vec4(diffuse, 1.0);;

    lowp vec3 emissive = mix_colors(material.specular.color, light_specular, material.specular.has_texture, sampler_specular);
    emissive += mix_colors(material.emissive.color, vec3(1,1,1), material.emissive.has_texture, sampler_emissive);
    FragData[OUT_EMISSIVE] = vec4(emissive, 1.0);;

    lowp vec3 fresnel = material.fresnel.color;
    if ( material.fresnel.has_texture )
        fresnel *= texture2D(sampler_fresnel, v_uv).rgb;
    fresnel *= pow(
        (1.0 - dot(v_surface_to_camera, bump_normal)),
        material.fresnel_exponent
    ) * smoothstep(0.0, 0.5, fresnel_val / fresnel_count);
    FragData[OUT_FRESNEL] = vec4(fresnel, 1.0);

    return diffuse + emissive + fresnel;
}



void main()
{
    if ( material.environment )
    {
        FragData[OUT_NORMAL] = vec4(v_norm, 1.0);
        FragData[OUT_ENVIRONMENT] = FragData[OUT_COMPOSITE] = vec4(environment_color(v_metallic_vector), 1.0);
        FragData[OUT_DIFFUSE] = FragData[OUT_EMISSIVE] = FragData[OUT_FRESNEL] = vec4(0.0);
    }
    else
    {
        lowp vec3 color = light_color();

        lowp float alpha = material.opacity;

        lowp vec3 environment = vec3(0);
        if ( material.index_of_refraction != 1.0 && alpha < 1.0 )
        {
            environment.rgb += environment_color(v_ior_vector) * (1.0 - alpha);
//             color = color_ior * (1.0 - alpha) + color * min(1.0, alpha * 2.0);
            alpha = 1.0;
        }

        if ( material.metallic > 0.0 )
        {
            environment.rgb += environment_color(v_metallic_vector) * material.metallic;
        }

        FragData[OUT_ENVIRONMENT] = vec4(environment.rgb, 1.0);
        color += environment;

        FragData[OUT_COMPOSITE] = vec4(color, alpha);
    }
}
