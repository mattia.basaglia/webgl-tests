// Precision hints:
// colors, [0,1] => lowp
// normals => mediump
// positions => highp
//
// Type | float range | float magnitude | int range
// high | -2^62, 2^62 | 2^-62, 2^62     | -2^16, 2^16
// med. | -2^14, 2^14 | 2^-14, 2^14     | -2^10, 2^10
// low  |  -2^8, 2^8  |  2^-8, 2^8      |  -2^8, 2^8

struct material_color
{
    lowp vec3 color;
    bool has_texture;
};
struct material_info
{
    material_color ambient;
    material_color diffuse;
    material_color specular;
    material_color emissive;
    material_color fresnel;
    lowp float specular_exponent;
    lowp float opacity;
    lowp float index_of_refraction;
    lowp float metallic;
    bool environment;
    lowp float displace_scale;
    lowp int displace_mode;
    lowp float fresnel_exponent;
};
uniform material_info material;

#define LIGHT_NUMBER 16
struct light_info {
    mediump vec3 direction;
    lowp vec4 color;
    lowp int type;
    lowp float strength;
    bool enabled;
};
uniform light_info lights[LIGHT_NUMBER];

#define M_PI 3.1415926535897932384626433832795

#if __VERSION__ >= 300
#   if defined(SHADER_VERTEX)
#       define varying out
#       define attribute in
#   elif defined(SHADER_FRAGMENT)
#       define varying in
#       define textureCube texture
#       define texture2D texture
        layout(location=0) out lowp vec4 FragData[6];
#       define gl_FragColor FragData[0]
#   endif
#else
#   if defined(SHADER_FRAGMENT)
#       define FragData gl_FragData
#   endif
#endif


varying lowp float v_light_brightness_diffuse[LIGHT_NUMBER];
varying lowp float v_light_brightness_specular[LIGHT_NUMBER];
varying mediump vec3 v_surface_to_light[LIGHT_NUMBER];
varying mediump vec3 v_norm;

varying lowp vec3 v_ambient_color;
varying mediump vec2 v_uv;
varying lowp vec3 v_ior_vector;
varying lowp vec3 v_metallic_vector;
varying mediump vec3 v_surface_to_camera;
varying highp vec3 v_world_position;

#line 1
