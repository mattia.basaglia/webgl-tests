import {Color, Point3D} from "./math.js";
import {CachedNgon, Texture, Vertex} from "./drawable.js";
import {Shader, shader_path} from "./shader.js";


export class FrameBuffer
{
    constructor(gl, width=null, height=null)
    {
        this.gl = gl;
        this.frbuf = gl.createFramebuffer();
        this.buffers = [];
        this.attachments = {};
        this.width = width !== null ? width * 2 : this.gl.canvas.clientWidth;
        this.height = height !== null ? height * 2 : this.gl.canvas.clientHeight;
    }

    bind()
    {
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frbuf);
        this.gl.drawBuffers(this.buffers.map(x => x.color_attachment).filter(x => x !== undefined));
    }

    unbind()
    {
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    }

    add_texture_color_buffer(attach_number, format=Texture.Format.rgba8)
    {
        var texture = new Texture(this.gl);
        texture.create_raw(this.width, this.height, null, format);

        if ( this.gl.version == 1 )
        {
            var sub = new FrameBuffer(this.gl, this.width, this.height);
            sub.attach_texture_color_buffer(texture, 0);
            sub.finalize();
        }

        this.attach_texture_color_buffer(texture, attach_number);
        return this;
    }

    attach_texture_color_buffer(texture, attach_number)
    {
        this.bind();
        texture.color_attachment = this.gl.COLOR_ATTACHMENT0 + attach_number;
        this.gl.framebufferTexture2D(
            this.gl.FRAMEBUFFER, texture.color_attachment,
            texture.target, texture.texture, 0
        );
        this.unbind();
        this.attachments[texture.color_attachment] = texture;
        this.buffers.push(texture);
        return this;
    }

    preset_build(n_color_buffers=1, depth_buffer=true, texture_depth=false)
    {
        for ( var i = 0; i < n_color_buffers; i++ )
            this.add_texture_color_buffer(i);

        if ( texture_depth )
            this.add_texture_depth_buffer();
        else if ( depth_buffer )
            this.add_depth_buffer();

        this.finalize();
        return this;
    }

    preset_build_multisample(n_color_buffers=1, depth_buffer=true, sample_size=4)
    {
        for ( var i = 0; i < n_color_buffers; i++ )
            this.add_multisample_color_buffer(i, sample_size);

        if ( depth_buffer )
            this.add_depth_buffer(sample_size);

        this.finalize();
        return this;
    }

    add_multisample_color_buffer(attach_number, sample_size=2)
    {
        this.bind();
        var buf = this.gl.createRenderbuffer();
        this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, buf);
        this.gl.renderbufferStorageMultisample(
            this.gl.RENDERBUFFER,
            sample_size,
            this.gl.RGBA8,
            this.width,
            this.height
        );
        buf.color_attachment = this.gl.COLOR_ATTACHMENT0 + attach_number;
        this.gl.framebufferRenderbuffer(
            this.gl.FRAMEBUFFER,
            buf.color_attachment,
            this.gl.RENDERBUFFER,
            buf
        );
        this.unbind();
        this.attachments[buf.color_attachment] = buf;
        this.buffers.push(buf);
        return this;
    }

    add_texture_depth_buffer()
    {
        this.bind();
        var texture = new Texture(this.gl);
        texture.texture = this.gl.createTexture();
        texture.target = this.gl.TEXTURE_2D;

        this.gl.bindTexture(texture.target, texture.texture);
        this.gl.texParameteri(texture.target, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
        this.gl.texParameteri(texture.target, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
        this.gl.texParameteri(texture.target, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(texture.target, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
        this.gl.texStorage2D(texture.target, 1, this.gl.DEPTH_COMPONENT16, this.width, this.height);
//         this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.DEPTH_COMPONENT,
//                            this.width, this.height, 0, this.gl.DEPTH_COMPONENT, this.gl.UNSIGNED_SHORT, null);

        this.gl.framebufferTexture2D(
            this.gl.FRAMEBUFFER, this.gl.DEPTH_ATTACHMENT,
            this.gl.TEXTURE_2D, texture.texture, 0
        );

        this.attachments[this.gl.DEPTH_ATTACHMENT] = texture;

        this.unbind();
        this.buffers.push(texture);
        return this;
    }

    add_depth_buffer(sample_size=1)
    {
        this.bind();
        var depth = this.gl.createRenderbuffer();
        this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, depth);
        if ( sample_size > 1 )
        {
            this.gl.renderbufferStorageMultisample(
                this.gl.RENDERBUFFER,
                sample_size,
                this.gl.DEPTH_COMPONENT16,
                this.width,
                this.height
            );
        }
        else
        {
            this.gl.renderbufferStorage(
                this.gl.RENDERBUFFER,
                this.gl.DEPTH_COMPONENT16,
                this.width,
                this.height
            );
        }
        this.gl.framebufferRenderbuffer(
            this.gl.FRAMEBUFFER,
            this.gl.DEPTH_ATTACHMENT,
            this.gl.RENDERBUFFER,
            depth
        );
        this.unbind();
        this.attachments[this.gl.DEPTH_ATTACHMENT] = depth;
        this.buffers.push(depth);
        return this;
    }

    finalize()
    {
        this.bind();
        var status = this.gl.checkFramebufferStatus(this.gl.FRAMEBUFFER);
        if ( status != this.gl.FRAMEBUFFER_COMPLETE )
        {
            const statuses = [
                "FRAMEBUFFER_INCOMPLETE_ATTACHMENT",
                "FRAMEBUFFER_INCOMPLETE_DIMENSIONS",
                "FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT",
                "FRAMEBUFFER_UNSUPPORTED"
            ];
            var err_name = "Unknown";
            for ( var st of statuses )
            {
                if ( status === this.gl[st] )
                {
                    err_name = st
                    break;
                }
            }
            console.warn("Frame buffer finalization error", status, err_name);
        }
        this.unbind();
        return this;
    }

    _resolve_attno(attachment_number)
    {
        if ( attachment_number >= this.gl.DEPTH_STENCIL_ATTACHMENT )
            return attachment_number;
        return this.gl.COLOR_ATTACHMENT0 + attachment_number;
    }

    bind_read(attachment_number)
    {
        if ( this.gl.version == 1 )
        {
            this.attachments[attachment_number].bind();
        }
        else
        {
            this.gl.bindFramebuffer(this.gl.READ_FRAMEBUFFER, this.frbuf);
            this.gl.readBuffer(this._resolve_attno(attachment_number));
        }
    }

    unbind_read(attachment_number=0)
    {
        if ( this.gl.version == 1 )
        {
            this.attachments[attachment_number].unbind();
        }
        else
        {
            this.gl.bindFramebuffer(this.gl.READ_FRAMEBUFFER, null);
        }
    }

    get_pixel(x, y, attachment_number=0)
    {
        this.bind_read(attachment_number);
        var pixbuf = new Uint8Array(4);
        this.gl.readPixels(x, y, 1, 1, this.gl.RGBA, this.gl.UNSIGNED_BYTE, pixbuf);
        this.unbind_read(attachment_number);
        return pixbuf;
    }

    copy_to_texture(texture, attachment_number=0)
    {
        this.bind_read(attachment_number);
        this.gl.bindTexture(texture.target, texture.texture);
        this.gl.copyTexImage2D(texture.target, 0, this.gl.RGBA, 0, 0, this.width, this.height, 0);
        this.unbind_read(attachment_number);
    }

    copy_to_frame_buffer(framebuffer, attachments=null)
    {
        this.gl.bindFramebuffer(this.gl.READ_FRAMEBUFFER, this.frbuf);
        this.gl.bindFramebuffer(this.gl.DRAW_FRAMEBUFFER, framebuffer.frbuf);

        if ( attachments === null )
            attachments = Object.keys(framebuffer.attachments).map(parseFloat);

        if ( attachments.includes(this.gl.DEPTH_ATTACHMENT) )
        {
            if ( this.has_depth_texture() && framebuffer.has_depth_texture() )
            {
                this.gl.clearBufferfv(this.gl.DEPTH, 0, [0, 0, 0, 0]);

                this.gl.blitFramebuffer(
                    0, 0, this.width, this.height,
                    0, 0, framebuffer.width, framebuffer.height,
                    this.gl.DEPTH_BUFFER_BIT,
                    this.gl.NEAREST
                );
            }

            attachments = attachments.filter(x => x !== this.gl.DEPTH_ATTACHMENT);
        }

        for ( var attach of attachments )
        {
            this.gl.readBuffer(attach);
            this.gl.drawBuffers(attachments.map(
                a => a != attach ? this.gl.NONE : a
            ));

            this.gl.blitFramebuffer(
                0, 0, this.width, this.height,
                0, 0, framebuffer.width, framebuffer.height,
                this.gl.COLOR_BUFFER_BIT,
                this.gl.NEAREST
            );
        }

        this.gl.bindFramebuffer(this.gl.DRAW_FRAMEBUFFER, null);
        this.gl.bindFramebuffer(this.gl.READ_FRAMEBUFFER, null);
    }

    has_depth_texture()
    {
        return this.attachments[this.gl.DEPTH_ATTACHMENT] instanceof Texture;
    }
}

/// EG: (new RenderPipeline(gl).default().framebuffer(6).postprocess("myfrag.glsl"))
export class RenderPipeline
{
    constructor(gl)
    {
        this.steps = [];
        this.gl = gl;
        this.named_steps = {};
    }

    render(scene)
    {
        var pv = new PipelineView(scene, this, this.steps.length-1);
        pv.invoke("render");
    }

    push(renderer)
    {
        this.steps.push(renderer);
        return this;
    }

    named(name)
    {
        this.named_steps[name] = this.steps[this.steps.length-1];
        return this;
    }

    lighting(owner=1)
    {
        return this.push(new LightUniforms(this.gl, owner));
    }

    default_renderer(settings={}, ...args)
    {
        var shader = Shader.from_uris(this.gl, ...args);
        return this.push(new DefaultRenderer(this.gl, shader, settings));
    }

    framebuffer(framebuffer, clear_color=null)
    {
        return this.push(new FramebufferRenderer(this.gl, framebuffer, clear_color));
    }

    new_framebuffer(n_color_buffers=1, depth_buffer=true, texture_depth=false, clear_color=null)
    {
        var fb = new FrameBuffer(this.gl);
        fb.preset_build(n_color_buffers, depth_buffer, texture_depth);
        return this.push(new FramebufferRenderer(this.gl, fb, clear_color));
    }

    new_framebuffer_multisample(...args)
    {
        var fb = new FrameBuffer(this.gl);
        fb.preset_build_multisample(...args);
        return this.push(new FramebufferRenderer(this.gl, fb));
    }

    postprocess(fragment=null, uniform={}, fetchers=pipebuf(-1))
    {
        var shader = Shader.from_uris(
            this.gl,
            "fb_vertex.glsl",
            fragment || "fb_fragment.glsl",
            "fb_common.glsl",
            shader_path+"fb_renderer/"
        );
        return this.push(new PostRenderer(this.gl, shader, uniform, fetchers));
    }

    post_effect(fragment=null, uniform={}, n_buffers=1)
    {
        return this.new_framebuffer(n_buffers).postprocess(fragment, uniform, pipebuf(-1));
    }

    kernel(kernel, kernel_scale=1, uniform={}, fetchers=pipebuf(-1))
    {
        return this.push(new KernelRenderer(this.gl, kernel, kernel_scale, uniform, fetchers));
    }

    kernel_effect(kernel, kernel_scale=1, uniform={})
    {
        return this.new_framebuffer(1).kernel(kernel, kernel_scale, uniform, pipebuf(-1))
    }

    blit(...args)
    {
        var fb = new FrameBuffer(this.gl);
        fb.preset_build(...args);
        return this.push(new BlitRenderer(this.gl, fb));
    }

    alt_scene(scene, pipeline)
    {
        return this.push(new SubPipeline(scene, pipeline));
    }

    blur(direction, radius, uniform={}, fetchers=pipebuf(-1))
    {
        return this.push(new DirectionalGaussianBlurRenderer(this.gl, direction, radius, uniform, fetchers));
    }

    dynamic_property(name, getter, setter)
    {
        Object.defineProperty(this, name, {
            configurable: false,
            enumerable: false,
            get: getter,
            set: setter,
        });
        return this;
    }
}

class PipelineView
{
    constructor(scene, pipeline, step_index)
    {
        this.scene = scene;
        this.pipeline = pipeline;
        this.step_index = step_index;
    }

    get step()
    {
        return this.pipeline.steps[this.step_index];
    }

    get_step(index)
    {
        if ( index > 0 )
            return this.pipeline.steps[index];
        else
            return this.pipeline.steps[index + this.step_index];
    }

    is_last()
    {
        return this.step_index == 0;
    }

    next()
    {
        return new PipelineView(this.scene, this.pipeline, this.step_index-1);
    }

    invoke_next(func)
    {
        if ( !this.is_last() )
            this.next().invoke(func);
    }

    invoke(func)
    {
        this.step[func](this);
    }
}

class Renderer
{
    constructor(gl)
    {
        this.gl = gl;
    }

    render(pipeline_view)
    {
        this.prepare_render(pipeline_view);
        this.on_render(pipeline_view)
    }

    prepare_render(pipeline_view)
    {
    }

    on_render(pipeline_view)
    {
    }

    draw_arrays(scene, buffers, mode, length)
    {
        buffers.forEach(b => b.send());
        this.gl.drawArrays(mode, 0, length);
    }

    create_buffer(flat, attribute_name, n_components)
    {
        const buffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, flat, this.gl.STATIC_DRAW);

        return new ShaderBuffer(
            buffer,
            this.shader.attribute[attribute_name],
            n_components,
        );
    }
}

class SubPipeline extends Renderer
{
    constructor(scene, pipeline)
    {
        super(scene.gl);
        this.scene = scene;
        this.pipeline = pipeline;
    }

    prepare_render(pipeline_view)
    {
        pipeline_view.invoke_next("render");
    }

    on_render(pipeline_view)
    {
        this.pipeline.render(this.scene);
    }
}

class LightUniforms extends Renderer
{
    constructor(gl, shader_owner)
    {
        super(gl);
        this.shader_owner = shader_owner;
    }

    prepare_render(pipeline_view)
    {
        pipeline_view.invoke_next("render");
    }

    on_render(pipeline_view)
    {
        var shader;
        if ( typeof this.shader_owner == "number" )
            shader = pipeline_view.pipeline.steps[pipeline_view.step_index+this.shader_owner].shader;
        else
            shader = pipeline_view.pipeline.named(this.shader_owner).shader;

        pipeline_view.scene.gl.useProgram(shader.program);
        for ( var light of pipeline_view.scene.lights )
            light.apply(shader);
        shader.uniform.light_ambient_color.set(scene.render_options.light_ambient_color);
    }
}


class DefaultRenderer extends Renderer
{
    constructor(gl, shader, {
        clear_color = true,
        clear_depth = true,
        enable_depth = true,
        enable_alpha = true,
    })
    {
        super(gl);
        this.shader = shader;

        this.clear_color = clear_color;
        this.clear_depth = clear_depth;
        this.enable_depth = enable_depth;
        this.enable_alpha = enable_alpha;
    }

    apply_gl_settings(scene)
    {
        var clear = 0;
        if ( this.clear_color )
        {
            this.gl.clearColor(...scene.render_options.bg_color.components);
            clear |= this.gl.COLOR_BUFFER_BIT;
        }
        if ( this.clear_depth )
        {
            this.gl.clearDepth(1.0);
            clear |= this.gl.DEPTH_BUFFER_BIT;
        }

        if ( clear )
            this.gl.clear( clear );

        if ( this.enable_depth )
        {
            this.gl.enable(this.gl.DEPTH_TEST);
            this.gl.depthFunc(this.gl.LEQUAL); // Near things obscure far things
        }
        else
        {
            this.gl.disable(this.gl.DEPTH_TEST);
        }

        if ( this.enable_alpha )
        {
            this.gl.enable(this.gl.BLEND)
            this.gl.blendFunc(this.gl.ONE, this.gl.ONE_MINUS_SRC_ALPHA);
        }
        else
        {
            this.gl.disable(this.gl.BLEND);
        }
    }

    on_render(pipeline_view)
    {
        this.apply_gl_settings(pipeline_view.scene);
        pipeline_view.scene.draw_clear();

        for ( var obj of pipeline_view.scene.objects )
            obj.render(pipeline_view.scene, this);

        pipeline_view.scene._log_error();
    }

    prepare_render(pipeline_view)
    {
        pipeline_view.invoke_next("render");
        this.prepare_scene(pipeline_view.scene);
    }

    prepare_scene(scene)
    {
        // Tell WebGL to use our program when drawing
        scene.gl.useProgram(this.shader.program);

        // Set the shader uniforms
        this.shader.uniform.projection_matrix.set(scene.camera.projection);
        this.shader.uniform.camera_matrix.set(scene.camera.transform);
        this.shader.uniform.camera_matrix_inv.set(scene.camera.transform.copy().invert());

        if ( scene.environment.target === scene.gl.TEXTURE_2D )
        {
            this.shader.uniform.env_mode.set(2);
            scene.environment.bind(this.shader.uniform.sampler_hdr.uniform, 0);
        }
        else if ( scene.environment.target === scene.gl.TEXTURE_CUBE_MAP )
        {
            this.shader.uniform.env_mode.set(1);
            scene.environment.bind(scene.shader.uniform.sampler_cube.uniform, 0);
        }
        else
        {
            // Work around https://bugzilla.mozilla.org/show_bug.cgi?id=1560767
            if ( scene.environment.hax === undefined )
            {
                scene.environment.hax = new Texture(scene.gl);
                scene.environment.hax.create(1, 1, new Color(0,0,0));
            }
            scene.environment.hax.bind(this.shader.uniform.sampler_hdr.uniform, -1);

            this.shader.uniform.env_mode.set(0);
        }
    }

    draw_arrays(scene, buffers, mode, length)
    {
        this.shader.uniform.model_view_matrix.set(scene.transform);

        var normal_matrix = scene.camera.transform.copy();
        normal_matrix.multiply(scene.transform);
        normal_matrix.invert();
        normal_matrix.transpose();
        this.shader.uniform.normal_matrix.set(normal_matrix);

        super.draw_arrays(scene, buffers, mode, length);
    }
}

class FramebufferRenderer extends Renderer
{
    constructor(gl, frame_buffer, clear_color=null)
    {
        super(gl);
        this.frame_buffer = frame_buffer;
        this.clear_color = clear_color;
    }

    prepare_render(pipeline_view)
    {
        pipeline_view.invoke_next("prepare_render");
    }

    on_render(pipeline_view)
    {
        this.frame_buffer.bind();
        if ( this.clear_color !== null )
        {
            this.gl.clearColor(...this.clear_color.components);
            this.gl.clear(this.gl.COLOR_BUFFER_BIT);
        }
        pipeline_view.invoke_next("on_render");
        this.frame_buffer.unbind();
    }
}

class BlitRenderer extends FramebufferRenderer
{
    on_render(pipeline_view)
    {
        super.on_render(pipeline_view);
        pipeline_view.get_step(-1).frame_buffer.copy_to_frame_buffer(this.frame_buffer);
    }
}

class PostRenderer extends Renderer
{
    constructor(gl, shader, uniform, fetchers)
    {
        super(gl);

        this.shader = shader;
        this.uniform = uniform;
        this.fetchers = fetchers;

        this.projector_sceen = new CachedNgon([
            new Vertex(new Point3D(-1, -1, 0)),
            new Vertex(new Point3D( 1, -1, 0)),
            new Vertex(new Point3D( 1,  1, 0)),
            new Vertex(new Point3D(-1,  1, 0)),
        ]);
    }

    prepare_render(pipeline_view)
    {
        pipeline_view.invoke_next("render");
        this.gl.useProgram(this.shader.program);
    }

    apply_uniforms()
    {
        for ( var k in this.uniform )
            this.shader.uniform[k].set(this.uniform[k]);
    }

    on_render(pipeline_view)
    {
        this.apply_uniforms();
        this.fetchers.bind_textures(this.shader, pipeline_view);
        this.projector_sceen.render(pipeline_view.scene, this);
    }
}

class KernelRenderer extends PostRenderer
{
    constructor(gl, kernel, kernel_scale, uniform, fetchers)
    {
        var shader = Shader.from_uris(gl, "fb_vertex.glsl", "kernel.glsl", "fb_common.glsl", shader_path+"fb_renderer/");
        shader.macros.kernel_sizesq = kernel.length;
        shader.macros.kernel_size = Math.sqrt(kernel.length);

        super(gl, shader, uniform, fetchers);
        this.kernel = kernel;
        this.kernel_scale = kernel_scale;
    }

    on_render(pipeline_view)
    {
        this.shader.uniform.kernel.set(this.kernel);
        this.shader.uniform.kernel_scale.set(this.kernel_scale);
        super.on_render(pipeline_view);
    }
}

export const kernel_presets = {
    "identity": [
        [
            0, 0, 0,
            0, 1, 0,
            0, 0, 0
        ],
        1
    ],
    "sharpen": [
        [
            0, -1, 0,
            -1, 5, -1,
            0, -1, 0
        ],
        1
    ],
    "edge_detect": [
        [
            -1, -1, -1,
            -1,  8, -1,
            -1, -1, -1
        ],
        1
    ],
    "gaussian5": [
        [
            1.0,  4.0,  6.0,  4.0, 1.0,
            4.0, 16.0, 24.0, 16.0, 4.0,
            6.0, 24.0, 36.0, 24.0, 6.0,
            4.0, 16.0, 24.0, 16.0, 4.0,
            1.0,  4.0,  6.0,  4.0, 1.0
        ],
        1 / 256
    ],
};

class DirectionalGaussianBlurRenderer extends PostRenderer
{
    constructor(gl, direction, radius, uniform, fetchers)
    {
        var shader = Shader.from_uris(gl, "fb_vertex.glsl", "gauss.glsl", "fb_common.glsl", shader_path+"fb_renderer/");
        super(gl, shader, uniform, fetchers);
        this._radius = radius;
        this.kernel = this.calculate_kernel(radius);
        this.direction = direction;
    }

    calculate_kernel(radius)
    {
        var kernel = [];
        // this gaussian has an integral of 1 (between -inf, +inf)
        var gauss = x => Math.exp(-Math.PI*x**2);
        // we truncate the tails of the gaussian curve, this is to add some of this back
        var to_add = 0.004;

        for ( var i = 0; i < radius + 1; i++ )
        {
            // x between 0 and 1, we evaluate the gaussian function at every x
            var x = i / radius;
            // y is the value of the gaussian at x
            var y = gauss(x);
            // approximate integral by dividing by radius and adding the tail, so it would sum to 1
            kernel.push((y + to_add) / radius);
        }

        // The kernel has values [a, b, c...] so that a + 2*b + 2*c ... = ~1
        // the 2 * is because we mirror it to the other side in the shader
        return kernel;
    }

    get radius()
    {
        return this._radius;
    }

    set radius(radius)
    {
        this._radius = radius;
        this.kernel = this.calculate_kernel(radius);
    }

    on_render(pipeline_view)
    {
        this.shader.uniform.kernel_size.set(this._radius);
        this.shader.uniform.kernel.set(this.kernel);
        this.shader.uniform.direction.set(this.direction);
        super.on_render(pipeline_view);
    }
}

class PipelineBuffers
{
    constructor()
    {
        this.fetchers = [];
    }

    p(...args)
    {
        if ( args.length == 0 )
        {
            // noop
        }
        else if ( args[0] instanceof TexFetcher )
        {
            this.fetchers = this.fetchers.concat(args);
        }
        else if ( args.length <= 3 && typeof args[0] == "number" )
        {
            this.fetchers.push(new TexFetcherPipelineBuffer(...args));
        }
        else if ( args[0] instanceof FrameBuffer )
        {
            this.fetchers.push(new TexFetcherFramebuffer(...args));
        }
        else if ( args[0] instanceof Texture )
        {
            this.fetchers.push(new TexFetcherTexture(...args));
        }
        else if ( typeof args[0] == "string" )
        {
            this.fetchers.push(new TexFetcherNamedPipelineBuffer(...args));
        }
        else
        {
            throw "Unknown texture fetcher";
        }
        return this;
    }

    bind_textures(shader, pipeline_view, start_unit = 0)
    {
        for ( var unit = 0; unit < this.fetchers.length; unit++ )
            this.fetchers[unit].bind_texture(shader, pipeline_view, unit+start_unit);
    }
}

export function pipebuf(...args)
{
    var fetcher = new PipelineBuffers();
    if ( args.length != 0 )
        fetcher.p(...args);
    return fetcher;
}

class TexFetcher
{
    constructor(uniform="texture_projector")
    {
        this.uniform = uniform;
    }

    get_texture(pipeline_view)
    {
    }

    bind_texture(shader, pipeline_view, bind_unit=0)
    {
        var uniform = shader.uniform;
        for ( var uni of this.uniform.split(".") )
            uniform = uniform[uni]
        this.get_texture(pipeline_view).bind(uniform.uniform, bind_unit);
    }
}

class TexFetcherPipelineBuffer extends TexFetcher
{
    constructor(pipeline_step=-1, buffer_number=0, ...args)
    {
        super(...args);
        this.pipeline_step = pipeline_step;
        this.buffer_number = buffer_number;
    }

    get_texture(pipeline_view)
    {
        var fb = pipeline_view.get_step(this.pipeline_step).frame_buffer;

        if ( this.buffer_number >= pipeline_view.scene.gl.DEPTH_STENCIL_ATTACHMENT )
            return fb.attachments[this.buffer_number];
        else
            return fb.buffers[this.buffer_number];
    }
}


class TexFetcherNamedPipelineBuffer extends TexFetcher
{
    constructor(name, buffer_number, ...args)
    {
        super(...args);
        this.buffer_number = buffer_number;
        this.name = name;
    }

    get_texture(pipeline_view)
    {
        var fb = pipeline_view.pipeline.named_steps[this.name].frame_buffer;

        if ( this.buffer_number >= pipeline_view.scene.gl.DEPTH_STENCIL_ATTACHMENT )
            return fb.attachments[this.buffer_number];
        else
            return fb.buffers[this.buffer_number];
    }
}

class TexFetcherFramebuffer extends TexFetcher
{
    constructor(frame_buffer, buffer_number=0, ...args)
    {
        super(...args);
        this.frame_buffer = frame_buffer;
        this.buffer_number = buffer_number;
    }

    get_texture(pipeline)
    {
        return this.frame_buffer.buffers[this.buffer_number];
    }
}

class TexFetcherTexture extends TexFetcher
{
    constructor(texture, ...args)
    {
        super(...args);
        this.texture = texture;
    }

    get_texture(pipeline)
    {
        return this.texture;
    }
}

class ShaderBuffer
{
    constructor(glbuffer, attribute, n_components)
    {
        this.buffer = glbuffer;
        this.attribute = attribute;
        this.n_components = n_components;
    }

    send()
    {
        if ( this.attribute !== undefined )
            this.attribute.set_buffer(this.buffer, this.n_components);
    }

    update_data(gl, array)
    {
        gl.bindBuffer(scene.gl.ARRAY_BUFFER, this.buffer);
        gl.bufferData(scene.gl.ARRAY_BUFFER, array, gl.STATIC_DRAW);
    }
}
