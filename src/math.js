export class Vector
{
    constructor(...components)
    {
        if ( this.constructor._size && components.length == 0 )
            this.components = new Float32Array(this.constructor._size)
        else
            this.components = Float32Array.from(components);
    }

    length()
    {
        return Math.hypot(...this.components);
    }

    length_squared()
    {
        return this.components.reduce((a, b) => a*a + b*b);
    }

    normalize()
    {
        var len = this.length();
        this.components.forEach((v, i, arr) => arr[i] /= len);
        return this;
    }

    flip()
    {
        this.components.forEach((v, i, arr) => arr[i] = -v);
        return this;
    }

    subtract(other)
    {
        this.components.forEach((v, i, arr) => arr[i] -= other.components[i]);
        return this;
    }

    add(other)
    {
        this.components.forEach((v, i, arr) => arr[i] += other.components[i]);
        return this;
    }

    divide(scalar)
    {
        this.components.forEach((v, i, arr) => arr[i] /= scalar);
        return this;
    }

    multiply(scalar)
    {
        this.components.forEach((v, i, arr) => arr[i] *= scalar);
        return this;
    }

    clone()
    {
        return new this.constructor(...this.components);
    }

    static define_named_component(name, index)
    {
        Object.defineProperty(this.prototype, name, {
            configurable: false,
            enumerable: false,
            get: function(){return this.components[index];},
            set: function(v){return this.components[index] = v;},
        });
    }

    static define_size(size)
    {
        this._size = size;
    }

    dot(other)
    {
        return (
            this.components
            .map((v, i) => v * other.components[i])
            .reduce((a, b) => a+b)
        );
    }

    lerp(other, factor)
    {
        return this.multiply(1-factor).add(other.clone().multiply(factor));
    }
}

export class Color extends Vector
{
    constructor(r, g, b, a=1.0)
    {
        super(r, g, b, a);
    }

    iarray()
    {
        return new Uint8Array(
            this.components.map(x => Math.round(x * 255))
        );
    }

    to_uint32()
    {
        // it's 1/6th faster to do this like this than map/reduce.
        // when generating textures it adds up
        return (
            Math.round(this.r * 255) |
            (Math.round(this.g * 255) << 8) |
            (Math.round(this.b * 255) << 16) |
            (Math.round(this.a * 255) << 24)
        ) >>> 0;
//         return (
//             this.components
//             .map((x, i) => Math.round(x * 255)  << (i*8))
//             .reduce((a, b) => a|b)
//             >>> 0
//         );
    }
}
Color.define_size(4);
Color.define_named_component("r", 0);
Color.define_named_component("g", 1);
Color.define_named_component("b", 2);
Color.define_named_component("a", 3);

export class Point3D extends Vector
{
    // only defined for 3D vectors
    cross_copy(other)
    {
        var a = this.components;
        var b = other.components;
        return new Point3D(
            a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0],
        );
    }
}
Point3D.define_size(3);
Point3D.define_named_component("x", 0);
Point3D.define_named_component("y", 1);
Point3D.define_named_component("z", 2);

export class Point2D extends Vector
{
}
Point2D.define_size(2);
Point2D.define_named_component("x", 0);
Point2D.define_named_component("y", 1);


export class Transform
{
    constructor()
    {
        this.clear();
    }

    translate(...args)
    {
        var point = this._point(args);
        var a = this.transform;
        a[12] = a[0] * point.x + a[4] * point.y + a[ 8] * point.z + a[12];
        a[13] = a[1] * point.x + a[5] * point.y + a[ 9] * point.z + a[13];
        a[14] = a[2] * point.x + a[6] * point.y + a[10] * point.z + a[14];
        a[15] = a[3] * point.x + a[7] * point.y + a[11] * point.z + a[15];

        return this;
    }

    _point(args)
    {
        return args.length == 3 ? new Point3D(...args) : args[0];
    }

    rotate(radians, ...args)
    {
        var axis = this._point(args);
        let len = Math.hypot(axis.x, axis.y, axis.z);
        if ( len < Number.EPSILON * 3 )
            return;
        // Normalized
        var x = axis.x/len;
        var y = axis.y/len;
        var z = axis.z/len;
        // sin/cos
        var s = Math.sin(radians);
        var c = Math.cos(radians);
        var t = 1 - c;

        var rot_matrix = new Transform();
        rot_matrix.transform[0] = x * x * t + c;
        rot_matrix.transform[1] = y * x * t + z * s;
        rot_matrix.transform[2] = z * x * t - y * s;

        rot_matrix.transform[4+0] = x * y * t - z * s;
        rot_matrix.transform[4+1] = y * y * t + c;
        rot_matrix.transform[4+2] = z * y * t + x * s

        rot_matrix.transform[8+0] = x * z * t + y * s
        rot_matrix.transform[8+1] = y * z * t - x * s
        rot_matrix.transform[8+2] = z * z * t + c;

        this.multiply(rot_matrix, 3);

        return this;
    }

    scale(...args)
    {
        var point = this._point(args);
        var a = this.transform;
        a[0] = a[0] * point.x;
        a[1] = a[1] * point.x;
        a[2] = a[2] * point.x;
        a[3] = a[3] * point.x;
        a[4] = a[4] * point.y;
        a[5] = a[5] * point.y;
        a[6] = a[6] * point.y;
        a[7] = a[7] * point.y;
        a[8] = a[8] * point.z;
        a[9] = a[9] * point.z;
        a[10] = a[10] * point.z;
        a[11] = a[11] * point.z;

        return this;
    }

    multiply(matrix, sz=4)
    {
        for ( var col = 0; col < sz; col++ )
        {
            var colval = [this.transform[col], this.transform[4+col], this.transform[8+col], this.transform[12+col]];
            for ( var row = 0; row < sz; row++ )
            {
                this.transform[row*4+col] = 0;
                for ( var o = 0; o < sz; o++ )
                {
                    this.transform[row*4+col] += colval[o] * matrix.transform[o+row*4];
                }
            }
        }

        return this;
    }

    lmultiply(matrix)
    {
        for ( var row = 0; row < 4; row++ )
        {
            var rowval = this.transform.slice(row*4, row*4+4);
            for ( var col = 0; col < 4; col++ )
            {
                this.transform[row*4+col] = 0;
                for ( var orow = 0; orow < 4; orow++ )
                {
                    this.transform[row*4+col] += rowval[orow] * matrix.transform[orow*4+col];
                }
            }
        }

        return this;
    }

    clear()
    {
        this.transform = new Float32Array(16);
        this.transform[0] = 1;
        this.transform[5] = 1;
        this.transform[10] = 1;
        this.transform[15] = 1;

        return this;
    }

    copy()
    {
        var c = new Transform();
        c.transform = Float32Array.from(this.transform)
        return c;
    }

    multiply_v4(v4)
    {
        var out = new Float32Array(4);
        for ( var i = 0; i < 4; i++ )
        {
            out[i] = 0;
            for ( var j = 0; j < 4; j++ )
            {
                out[i] += this.transform[i*4+j] * v4[j];
            }
        }
        return out;
    }

    lmultiply_v4(v4)
    {
        var out = new Float32Array(4);
        for ( var i = 0; i < 4; i++ )
        {
            out[i] = 0;
            for ( var j = 0; j < 4; j++ )
            {
                out[i] += this.transform[i+j*4] * v4[j];
            }
        }
        return out;
    }

    invert()
    {
        // plainly copied from mat4.js 'cause I CBA to implement it properly
        var a = this.transform;
        let a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3];
        let a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7];
        let a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11];
        let a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

        let b00 = a00 * a11 - a01 * a10;
        let b01 = a00 * a12 - a02 * a10;
        let b02 = a00 * a13 - a03 * a10;
        let b03 = a01 * a12 - a02 * a11;
        let b04 = a01 * a13 - a03 * a11;
        let b05 = a02 * a13 - a03 * a12;
        let b06 = a20 * a31 - a21 * a30;
        let b07 = a20 * a32 - a22 * a30;
        let b08 = a20 * a33 - a23 * a30;
        let b09 = a21 * a32 - a22 * a31;
        let b10 = a21 * a33 - a23 * a31;
        let b11 = a22 * a33 - a23 * a32;

        // Calculate the determinant
        let det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

        if ( !det )
            return null;

        det = 1.0 / det;

        a[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
        a[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
        a[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
        a[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
        a[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
        a[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
        a[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
        a[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
        a[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
        a[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
        a[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
        a[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
        a[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
        a[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
        a[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
        a[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

        return this;
    }

    transpose()
    {
        var swap = function(x, y)
        {
            var t = this.transform[x];
            this.transform[x] = this.transform[y];
            this.transform[y] = t;
        }.bind(this);
        swap(0+1, 4+0);
        swap(0+2, 8+0);
        swap(0+3, 12+0);
        swap(4+2, 8+1);
        swap(4+3, 12+1);
        swap(8+3, 12+2);

        return this;
    }

    project(point)
    {
        var p4 = this.lmultiply_v4(Float32Array.of(...point.components, 1.0));
        var w = 1.0/p4[2];

        return new Point3D(
            p4[0] * w,
            p4[1] * w,
            p4[2] * w,
        );
    }

    _extract(mat)
    {
        if ( mat instanceof Transform )
            return mat.transform;
        else if ( mat instanceof Float32Array )
            return mat;
        throw "Unsupported type";
    }

    set_to_perspective(fovy, aspect, z_near, z_far)
    {
        let f = 1.0 / Math.tan(fovy / 2);
        this.transform = new Float32Array(16);
        var out = this.transform;
        out[0] = f / aspect;
        out[5] = f;
        out[11] = -1;

        if ( z_far != null && z_far !== Infinity )
        {
            var nf = 1 / (z_near - z_far);
            out[10] = (z_far + z_near) * nf;
            out[14] = (2 * z_far * z_near) * nf;
        }
        else
        {

            out[10] = -1;
            out[14] = -2 * z_near;
        }

        return this;
    }

    set_to_ortho(out, left, right, bottom, top, near, far)
    {
        this.transform = new Float32Array(16);
        var out = this.transform;
        let lr = 1 / (left - right);
        let bt = 1 / (bottom - top);
        let nf = 1 / (near - far);

        out[0] = -2 * lr;
        out[5] = -2 * bt;
        out[10] = 2 * nf;
        out[12] = (left + right) * lr;
        out[13] = (top + bottom) * bt;
        out[14] = (far + near) * nf;
        out[15] = 1;

        return this;
    }
}

export class TransformStack extends Transform
{
    constructor()
    {
        super();
        this.stack = [];
    }

    push()
    {
        this.stack.push(Float32Array.from(this.transform));
    }

    pop()
    {
        this.transform = this.stack.pop();
    }
}
