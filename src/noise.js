import {Color, Point2D} from "./math.js";
import {Texture} from "./drawable.js";

function lerp(a, b, f)
{
    return (1 - f) * a + f * b;
}

export function simple_prng(seed = 1)
{
    var _seed = Math.max(1, seed);
    var max = 2147483647;
    return function ()
    {
        _seed = _seed * 16807 % max;
        return _seed / max;
    };
}

function noisepoint_to_color(x)
{
    return Math.abs(x) * 255;
}

function noisepoint_to_color_rgba(x)
{
    var comp = Math.round(noisepoint_to_color(x));
    return comp | (comp << 8) | (comp << 16) | 0xff000000;
}

export class NoiseToColorGradient
{
    constructor(colors=[], scale=0.5, offset=0.5)
    {
        this.scale = scale;
        this.offset = offset;
        this.weight = colors.length;
        this.colors = colors.map(c => [1, c]);
        this.to_color = this._to_color.bind(this);
    }

    add_color(color, weight=1)
    {
        this.colors.push([weight, color]);
        this.weight += weight;
    }

    get_color(noise_value)
    {
        if ( this.colors.length == 0 )
            return 0x00000000;

        var val = noise_value * this.scale + this.offset * this.weight;

        if ( val <= 0 )
            return this.colors[0][1];
        if ( val >=  this.weight )
            return this.colors[this.colors.length-1][1];

        var c0 = this.colors[0][1];
        var c1 = this.colors[0][1];
        for ( var [weight, col] of this.colors )
        {
            c0 = c1;
            c1 = col;
            if ( val < weight )
            {
                val /= weight;
                break;
            }
            val -= weight;
        }

        return new Color(lerp(c0.r, c1.r, val), lerp(c0.g, c1.g, val), lerp(c0.b, c1.b, val), lerp(c0.a, c1.a, val));
    }

    _to_color(noise_value)
    {
        return this.get_color(noise_value).to_uint32();
    }
}

export function noise_to_texture(flat_data, gl, tex_width, tex_height, to_color=noisepoint_to_color)
{
    if ( flat_data.length < tex_width * tex_height )
        throw "Not enough data";

    var format = Texture.Format.grayscale8;
    let data = new Uint8Array(flat_data.map(to_color));
    var texture = new Texture(gl);
    texture.create_raw(tex_width, tex_height, data, format);



    return texture;
}

export function noise_to_texture_rgba(flat_data, gl, tex_width, tex_height, to_color=noisepoint_to_color_rgba)
{
    if ( flat_data.length < tex_width * tex_height )
        throw "Not enough data";

    var format = Texture.Format.rgba8;
    let buffer = new ArrayBuffer(tex_width * tex_height * 4);
    let uint8data = new Uint8Array(buffer);
    var rgb_data = new Uint32Array(buffer);
    rgb_data.set(flat_data.map(to_color), 0);
    var texture = new Texture(gl);
    texture.create_raw(tex_width, tex_height, uint8data, format);

    return texture;
}

class NoiseGenerator
{
    value(...args)
    {
        return 0;
    }

    _data_step(top_left, bottom_right, n_samples_x, n_samples_y)
    {
        if ( n_samples_y === null )
            n_samples_y = n_samples_x;

        return new Point2D(
            (bottom_right.x - top_left.x) / n_samples_x,
            (bottom_right.y - top_left.y) / n_samples_y,
        );

    }

    data_2d(top_left, bottom_right, n_samples_x, n_samples_y=null)
    {
        var step = this._data_step(top_left, bottom_right, n_samples_x, n_samples_y);
        var out = [];
        for ( var y = top_left.y; y < bottom_right.y; y += step.y )
        {
            var row = [];
            out.push(row);
            for ( var x = top_left.x; x < bottom_right.x; x += step.x )
            {
                row.push(this.value(x, y));
            }
        }
        return out;
    }

    data_flat(top_left, bottom_right, n_samples_x, n_samples_y=null)
    {
        var step = this._data_step(top_left, bottom_right, n_samples_x, n_samples_y);
        var out = [];
        for ( var y = top_left.y; y < bottom_right.y; y += step.y )
            for ( var x = top_left.x; x < bottom_right.x; x += step.x )
                out.push(this.value(x, y));
        return out;
    }

    generate_texture(top_left, bottom_right, gl, tex_width, tex_height, to_color=noisepoint_to_color)
    {
        var flat_data = this.data_flat(top_left, bottom_right, tex_width, tex_height);
        return noise_to_texture(flat_data, gl, tex_width, tex_height, to_color);
    }

    generate_texture_rgba(top_left, bottom_right, gl, tex_width, tex_height, to_color=noisepoint_to_color_rgba)
    {
        var flat_data = this.data_flat(top_left, bottom_right, tex_width, tex_height);
        return noise_to_texture_rgba(flat_data, gl, tex_width, tex_height, to_color);
    }
}

export class DynamicWhiteNoise extends NoiseGenerator
{
    constructor(rng=Math.random)
    {
        super();
        this.rng = rng;
    }

    value()
    {
        return this.rng();
    }

    data_linear(count)
    {
        var out = [];
        for ( var x = 0; x < count; x++ )
            out.push(this.value());
        return out;
    }
}

export class Prelin extends NoiseGenerator
{
    /**
     * \param rng       Callable that returns a random value between 0 and 1
     * \param amplitude The values will be in [-amplitude, amplitude]
     * \param frequency Scales coordinates
     */
    constructor(frequency=1, amplitude=1, rng=Math.random)
    {
        super();
        this.rng = rng;
        this.gradients = new Map();
        this.amplitude = amplitude;
        this.frequency = frequency;
    }

    get_gradient(x, y)
    {
        var key = `${x},${y}`;
        var val = this.gradients.get(key);
        if ( val === undefined )
        {
            var angle = Math.PI * 2 * this.rng();
            val = new Point2D(Math.cos(angle), Math.sin(angle));
            this.gradients.set(key, val);
        }
        return val;
    }

    dot_grid(ix, iy, x, y)
    {
        var dx = x - ix;
        var dy = y - iy;
        var grad = this.get_gradient(iy, ix);
        return dx * grad.x + dy * grad.y;
    }

    ease(p)
    {
        return 3*p**2 - 2*p**3;
    }

    value(x, y)
    {
        x *= this.frequency;
        y *= this.frequency;
        var x0 = Math.floor(x);
        var y0 = Math.floor(y);
        var x1 = x0 + 1;
        var y1 = y0 + 1;

        var s = this.dot_grid(x0, y0, x, y);
        var t = this.dot_grid(x1, y0, x, y);
        var u = this.dot_grid(x0, y1, x, y);
        var v = this.dot_grid(x1, y1, x, y);
        var sx = this.ease(x - x0);
        var sy = this.ease(y - y0);
        var value = lerp(lerp(s, t, sx), lerp(u, v, sx), sy);
        return value * this.amplitude;
    }
}

export class MultiPrelin extends NoiseGenerator
{
    constructor(default_rng=Math.random)
    {
        super();
        this.octaves = [];
        this.default_rng = default_rng;
        this.total_amplitude = 0;
    }

    octave(frequency, amplitude=1, rng=null)
    {
        amplitude = Math.abs(amplitude);
        this.octaves.push(new Prelin(frequency, amplitude, rng || this.default_rng));
        this.total_amplitude += amplitude;
        return this;
    }

    value(x, y)
    {
        var value = 0;
        for ( var oct of this.octaves )
        {
            value += oct.value(x, y) / this.total_amplitude;
        }
        return value;
    }
}

export const distances = Object.freeze({
    p2: Math.hypot,
    pinf: (...args) => Math.max(...args.map(Math.abs)),
    p1: (...args) => args.map(Math.abs).reduce((a,v) => a+v),
});

export function pnorm(p)
{
    return (...args) => args.map(a => Math.abs(a) ** p).reduce((a,v) => a+v) ** (1/p);
}

export class Voronoi extends NoiseGenerator
{
    /**
     * \param rng       Callable that returns a random value between 0 and 1
     * \param distance  Distance function
     * \param amplitude The values will be in [-amplitude, amplitude]
     * \param frequency Scales coordinates
     */
    constructor(frequency=1, amplitude=1, distance=distances.p2, rng=Math.random)
    {
        super();
        this.rng = rng;
        this.gradients = new Map();
        this.amplitude = amplitude;
        this.frequency = frequency;
        this.distance = distance;
        this.radius = 0.5;
        this.max_dist = this.distance(this.radius, this.radius) + this.radius;
    }

    get_gradient(x, y)
    {
        var key = `${x},${y}`;
        var val = this.gradients.get(key);
        if ( val === undefined )
        {
            var angle = Math.PI * 2 * this.rng();
            val = new Point2D(x + Math.cos(angle) * this.radius, y + Math.sin(angle) * this.radius);
            this.gradients.set(key, val);
        }
        return val;
    }

    get_distance(x, y, xi, yi)
    {
        var grad = this.get_gradient(xi, yi);
        return this.distance(x - grad.x, y - grad.y);
    }

    value(x, y)
    {
        x *= this.frequency;
        y *= this.frequency;
        var x0 = Math.round(x);
        var y0 = Math.round(y);

        var dist = Infinity;
        for ( var dy = -1; dy <= 1; dy++ )
            for ( var dx = -1; dx <= 1; dx++ )
            {
                var d = this.get_distance(x, y, x0 + dx, y0 + dy);
                if ( d < dist )
                    dist = d;
            }

        var value = dist / this.max_dist;
        return value * this.amplitude;

    }
}

export class PatternTurbulence extends NoiseGenerator
{
    constructor(pattern_func, frequency=1, amplitude=1, turbulence_strength=1.5,
                noise_generator=new Prelin())
    {
        super();
        this.gradients = new Map();
        this.amplitude = amplitude;
        this.frequency = frequency;
        this.turbulence_strength = turbulence_strength;
        this.noise_generator = noise_generator;
        this.pattern_func = pattern_func;
    }

    turbulence(x, y)
    {
        return this.noise_generator.value(x, y) * this.turbulence_strength;
    }

    value(x, y)
    {
        x *= this.frequency;
        y *= this.frequency;
        return this.pattern_func(x, y, this) * this.amplitude;
    }
}

export const noise_presets = Object.freeze({
    wood_x: function(rng=Math.random){
        var noise = ((new MultiPrelin(rng))
            .octave(1, 1).octave(5, 0.2)
        );
        function pattern(x, y, gen)
        {
            return Math.sin(Math.sin(x) + gen.turbulence(x, y));
        }
        return new PatternTurbulence(pattern, 25, 1, 1.1, noise);
    },
    marble: function(rng=Math.random){
        var noise = ((new MultiPrelin(rng))
            .octave(1, 1).octave(2, 0.5).octave(5, 0.2)
        );
        function pattern(x, y, gen)
        {
            return (1 + Math.sin( (x + gen.turbulence(x * 5 , y * 5 ) / 2 ) * 50) ) / 2;
        }
        return new PatternTurbulence(pattern, 1, 1, 0.75, noise);
    },
    wood_rings: function(rng=Math.random){
        var noise = ((new MultiPrelin(rng))
            .octave(1, 1).octave(5, 0.2)
        );
        function pattern(x, y, gen)
        {
            return Math.sin(Math.hypot(x, y) + gen.turbulence(x, y));
        }
        return new PatternTurbulence(pattern, 25, 1, 1.1, noise);
    },

});
