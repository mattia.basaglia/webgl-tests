import {Point2D, Point3D, Color, Transform} from "./math.js";
import {downloader} from "./downloader.js";


export class Vertex
{
    constructor(point, uv=new Point2D(0,0), normal=new Point3D(0, 0, 1))
    {
        this.point = point;
        this.uv = uv;
        this.normal = normal;
    }
}

export class Material
{
    constructor(params={})
    {
        this.colors = [];
        this.misc = [];
        this._get_param_color(params, "ambient", new Color(1, 1, 1));
        this._get_param_color(params, "diffuse", new Color(1, 1, 1));
        this._get_param_color(params, "specular", new Color(1, 1, 1));
        this._get_param_color(params, "emissive", new Color(0, 0, 0));

        this._get_param_misc(params, "specular_exponent", 12);
        this._get_param_misc(params, "opacity", 1);
        this._get_param_misc(params, "index_of_refraction", 1);

        // TODO load from mtl
        this._get_param_misc(params, "metallic", 0);
        this._get_param_misc(params, "environment", false);
        this._get_param_misc(params, "displace_mode", 0);
        this._get_param_misc(params, "displace_scale", 1);
        this._get_param(params, "displace_map", null);

        this._get_param_color(params, "fresnel", new Color(0, 0, 0));
        this._get_param_misc(params, "fresnel_exponent", 1);

    }

    _get_param(params, name, default_value)
    {
        this[name] = params[name] === undefined ? default_value : params[name];
    }

    _get_param_color(params, name, default_value)
    {
        this._get_param(params, name, default_value);
        this._get_param(params, name + "_texture", null);
        this.colors.push(name);
    }

    _get_param_misc(params, name, default_value)
    {
        this._get_param(params, name, default_value);
        this.misc.push(name);
    }

    apply_to_scene(scene, renderer)
    {
        var texbind = 1;
        for ( var color of this.colors )
        {
            var tex = this[color + "_texture"];
            renderer.shader.uniform.material[color].color.set(this[color]);
            renderer.shader.uniform.material[color].has_texture.set(tex ? 1 : 0);
            this._apply_texture(scene, renderer, tex, "sampler_" + color, texbind++);
        }

        for ( var oth of this.misc )
            renderer.shader.uniform.material[oth].set(this[oth]);

        if ( this.displace_mode && renderer.shader.macros.BUMP_ENABLED )
            this._apply_texture(scene, renderer, this.displace_map, "sampler_displace", texbind++);
    }

    _apply_texture(scene, renderer, tex, uniform_name, texbind)
    {
        if ( tex )
        {
            if ( !(tex instanceof Texture) )
                tex = scene.textures[tex];
            tex.bind(renderer.shader.uniform[uniform_name].uniform, texbind);
        }

    }

    static shadeless(color=new Color(1, 1, 1))
    {
        return new Material({
            emissive: color,
            diffuse: new Color(0, 0, 0),
            specular: new Color(0, 0, 0),
            ambient: new Color(0, 0, 0),
        });
    }
}

Material.DisplaceMode = Object.freeze({
    NONE: 0,
    BUMP: 1,
    NORMAL: 2,
});

/**
 * \brief Base class for objects to be rendered in a scene
 */
export class Drawable3D
{
    constructor(material=null, transform=null)
    {
        this.material = material;
        this.transform = transform;
    }

    render(scene, renderer)
    {
        this.on_render_enter(scene, renderer);
        this.on_render(scene, renderer);
        this.on_render_leave(scene, renderer);
    }

    on_render_enter(scene, renderer)
    {
        if ( this.transform )
        {
            scene.transform.push();
            scene.transform.multiply(this.transform);
        }

        if ( this.material )
            this.material.apply_to_scene(scene, renderer);
    }

    on_render_leave(scene, renderer)
    {
        if ( this.transform )
            scene.transform.pop();
    }

    get_vertices()
    {
        return [];
    }
}

function ngon_normal(vertices)
{
    var normal = new Point3D(0, 0, 0);
    for ( var i = 0; i < vertices.length; i++ )
    {
        var current = vertices[i].point;
        var next = vertices[(i+1) % vertices.length].point;
        normal.x += (current.y - next.y) * (current.z + next.z);
        normal.y += (current.z - next.z) * (current.x + next.x);
        normal.z += (current.x - next.x) * (current.y + next.y);
    }

   normal.normalize();
   return normal;
}

/// Converts an ngon into fanned triangles
function ngon_to_fan(vertices, auto_normals=false, output=[])
{
    if ( vertices.len < 3 )
        throw "Needs at least 3 Vertices"

    var normal = null;
    if ( auto_normals )
        normal = ngon_normal(vertices);

    var a = vertices.shift();
    output.push(a);
    output.push(vertices[0]);
    output.push(vertices[1]);
    var last = vertices[1];

    if ( auto_normals )
        a.normal = vertices[0].normal = vertices[1].normal = normal;

    for ( var v of vertices.slice(2) )
    {
        if ( auto_normals )
            v.normal = normal;
        output.push(a);
        output.push(last);
        output.push(v);
        last = v;
    }

    return output;
}

export const DrawMode = Object.freeze({
    POINTS: 0,
    LINES: 1,
    LINE_LOOP: 2,
    LINE_STRIP: 3,
    TRIANGLES: 4,
    TRIANGLE_STRIP: 5,
    TRIANGLE_FAN: 6,
});

export class PointArray extends Drawable3D
{
    constructor(points=[], material=new Material(), mode=DrawMode.TRIANGLES)
    {
        super(material, new Transform());
        this.points = points; /// \todo Rename into vertices
        this.mode = mode;
    }

    push(vertex)
    {
        this.points.push(vertex);
    }

    push_fan(vertices, auto_normals=false)
    {
        this.push_face(vertices, auto_normals);
    }

    push_face(vertices, auto_normals=false)
    {
        ngon_to_fan(vertices, auto_normals, this.points);
    }

    on_render(scene, renderer)
    {
        var buf = this.render_get_buffers(renderer);
        renderer.draw_arrays(scene, buf, this.mode, this.points.length);
    }

    _mkbuf(renderer, component, attribute, n_components)
    {
        var flatarr = new Float32Array(n_components * this.points.length);
        this.points.map(y=>y[component].components).forEach((x, i) => flatarr.set(x, i*n_components));
        return renderer.create_buffer(flatarr, attribute, n_components);
    }

    render_get_buffers(renderer)
    {
        return [
            this._mkbuf(renderer, "point", "vertex_position", 3),
            this._mkbuf(renderer, "normal", "vertex_normal", 3),
            this._mkbuf(renderer, "uv", "vertex_uv", 2),
        ];
    }

    get_vertices()
    {
        return this.points;
    }
}

export class CachedPointArray extends PointArray
{
    update_buffers(scene, renderer)
    {
        this.buffers = super.render_get_buffers(scene, renderer);
    }

    render_get_buffers(scene, renderer)
    {
        if ( !this.buffers )
        {
            this.update_buffers(scene, renderer);
        }
        return this.buffers;
    }
}

export class Ngon extends Drawable3D
{
    constructor(vertices=[], material=null, auto_close=true, mode=DrawMode.TRIANGLE_FAN)
    {
        super(material, null);
        this.vertices = vertices;
        this.auto_close = auto_close;
        this.mode = mode;
    }

    recalculate_normal()
    {
        var normal = ngon_normal(this.vertices);
        for ( var vertex of this.vertices )
            vertex.normal = normal;
    }

    on_render(scene, renderer)
    {
        var len = this.vertices.length;
        if ( this.auto_close )
            len += 1;
        var buf = this.render_get_buffers(scene, renderer);
        renderer.draw_arrays(scene, buf, this.mode, len);
    }

    _mkbuf(renderer, component, attribute, n_components)
    {
        var flat = this.vertices.map(y=>y[component].components);
        if ( this.auto_close )
            flat.push(flat[0]);
        var flatarr = new Float32Array(n_components * flat.length);
        flat.forEach((x, i) => flatarr.set(x, i*n_components));
        return renderer.create_buffer(flatarr, attribute, n_components);
    }

    render_get_buffers(scene, renderer)
    {
        return [
            this._mkbuf(renderer, "point", "vertex_position", 3),
            this._mkbuf(renderer, "normal", "vertex_normal", 3),
            this._mkbuf(renderer, "uv", "vertex_uv", 2),
        ];
    }

    get_vertices()
    {
        return this.vertices;
    }

    set wireframe(wf)
    {
        this.mode = wf ? DrawMode.LINE_STRIP : DrawMode.TRIANGLE_FAN;
    }
}

export class CachedNgon extends Ngon
{
    /// TODO: mixin?
    update_buffers(scene, renderer)
    {
        this.buffers = super.render_get_buffers(scene, renderer);
    }

    render_get_buffers(scene, renderer)
    {
        if ( !this.buffers )
        {
            this.update_buffers(scene, renderer);
        }
        return this.buffers;
    }
}

/**
 * \brief Nestable drawable object
 */
export class Model extends Drawable3D
{
    constructor(children=[], material=null, transform=new Transform())
    {
        super(material, transform);
        this.children = children;
    }

    on_render(scene, renderer)
    {
        for ( var p of this.children )
            p.render(scene, renderer);
    }

    push(child)
    {
        this.children.push(child);
    }

    push_face(vertices, material=null, auto_normals=false)
    {
        this.push(new CachedNgon(vertices, material));
        if ( auto_normals )
            this.children[this.children.length-1].recalculate_normal();

    }

    push_line(vertices, material=null)
    {
        this.push(new PointArray(
            vertices.map(x=>new Vertex(x)),
            material,
            DrawMode.LINE_STRIP
        ));
    }

    push_points(vertices, material=null)
    {
        this.push(new PointArray(
            vertices.map(x=>new Vertex(x)),
            material,
            DrawMode.POINTS
        ));
    }


    get_vertices()
    {
        return this.children.map(x=>x.get_vertices()).reduce(
            (a,b) => a.concat(b),
            []
        );
    }
}

export class Texture
{
    constructor(gl)
    {
        this.gl = gl;
        this.texture = null;
        this.video_element = null;
        this.target = null;
    }

    create_raw(width, height, data, format=null)
    {
        this.texture = this.gl.createTexture();
        this.target = this.gl.TEXTURE_2D;
        if ( format === null )
            format = Texture.Format.rgba8;

        const level = 0;
        const border = 0;
        this.gl.bindTexture(this.target, this.texture);
        this.gl.texImage2D(
            this.target,
            level,
            this.gl[format.ifmt],
            width,
            height,
            border,
            this.gl[format.fmt],
            this.gl[format.type],
            data
        );
        this.gl.texParameteri(this.target, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.target, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.target, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.target, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
//         this.gl.texParameteri(this.target, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
//         this.gl.texParameteri(this.target, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
    }

    create(width, height, color)
    {
        var data = null;
        if ( color )
        {
            var base_data = Array.from(color.iarray());
            data = [];
            for ( var i = 0; i < width * height + 1; i++ )
                data = data.concat(base_data);
            data = new Uint8Array(data);
        }
        this.create_raw(width, height, data);
    }

    from_uri(uri)
    {
        this.create(1, 1, new Color(0, 0, 1));
        var on_load = function(job)
        {
            this.gl.bindTexture(this.target, this.texture);
            this.gl.texImage2D(
                this.target,
                0,
                this.gl.RGBA,
                this.gl.RGBA,
                this.gl.UNSIGNED_BYTE,
                job.image
            );

            // WebGL1 has different requirements for power of 2 images
            // vs non power of 2 images so check if the image is a
            // power of 2 in both dimensions.
            if ( is_power_of_2(job.image.width) && is_power_of_2(job.image.height) )
            {
                // Yes, it's a power of 2. Generate mips.
                this.gl.generateMipmap(this.target);
            }
            else
            {
                // No, it's not a power of 2. Turn off mips and set
                // wrapping to clamp to edge
                this.gl.texParameteri(this.target, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
                this.gl.texParameteri(this.target, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
                this.gl.texParameteri(this.target, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
                this.gl.texParameteri(this.target, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
            }
        }.bind(this);
        downloader.download_image(uri, on_load, {});
    }

    from_video_uri(uri)
    {
        this.create(1, 1, new Color(0, 0, 1));

        this.gl.texParameteri(this.target, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.target, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.target, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);

        downloader.download_video(uri, function(job){
            this.video_element = job.video;
            job.video.muted = true;
            job.video.loop = true;
            job.video.play();
        }.bind(this), {});
    }

    update()
    {
        if ( !this.video_element )
            return;
        this.gl.bindTexture(this.target, this.texture);
        this.gl.texImage2D(
            this.target,
            0,
            this.gl.RGBA,
            this.gl.RGBA,
            this.gl.UNSIGNED_BYTE,
            this.video_element
        );
    }

    bind(sampler, unit=0)
    {
        // For some reason unit 0 doesn't work when using a TEXTURE_2D
        // for the environment map
        unit++;
        var tunit = `TEXTURE${unit}`;
        this.gl.activeTexture(this.gl[tunit]);
        this.gl.bindTexture(this.target, this.texture);
        this.gl.uniform1i(sampler, unit);
    }


    from_cubemap_uris(...uris)
    {
        var group = `cubemap_${downloader.new_sender_id()}`;
        for ( let i = 0; i < 6; i++ )
        {
            var on_load = function(job)
            {
                if ( job.downloader.group_is_completed(group) )
                {
                    this.create_cubemap(
                        job.downloader.groups[group]
                        .map(j => j.image)
                    );
                    job.downloader.clear_group(group);
                }
            }.bind(this)

            downloader.download_image(uris[i], on_load, {}, group);
        }
    }

    create_cubemap(image_objects)
    {
        this.texture = this.gl.createTexture();
        this.target = this.gl.TEXTURE_CUBE_MAP;
        this.gl.bindTexture(this.target, this.texture);

        const targets = [
            this.gl.TEXTURE_CUBE_MAP_POSITIVE_X,
            this.gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
            this.gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
            this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
            this.gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
            this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
        ];

        for ( var i = 0; i < targets.length; i++ )
        {
            this.gl.texImage2D(targets[i], 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, image_objects[i]);
        }
        this.gl.generateMipmap(this.target);
        this.gl.texParameteri(this.target, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_LINEAR);
    }
}

Texture.Format = Object.freeze({
    rgba8: {
        ifmt: "RGBA",
        fmt: "RGBA",
        type: "UNSIGNED_BYTE",
        array: Uint8Array,
        components: 3,
    },
    grayscale8: {
        ifmt: "LUMINANCE",
        fmt: "LUMINANCE",
        type: "UNSIGNED_BYTE",
        array: Uint8Array,
        components: 1,
    },
    rgbaf16: {
        ifmt: "RGBA16F",
        fmt: "RGBA",
        type: "FLOAT",
        array: Float32Array,
        components: 3,
    },
});


function is_power_of_2(value)
{
    return (value & (value - 1)) == 0;
}

export class Light // extends Drawable3D ?
{
    constructor(
        type=Light.Type.POINT,
        color=new Color(1, 1, 1),
        position=new Point3D(0, 0, 0),
        point_strength=100.0,
        index=null,
        enabled=true)
    {
        this.type = type;
        this.color = color;
        this.position = position;
        this.point_strength = point_strength;
        this.index = index !== null ? index : Light._index++;
        this.enabled = enabled;
    }

    get direction()
    {
        return this.position;
    }

    set direction(v)
    {
        return this.position = v;
    }

    apply(shader)
    {
        shader.uniform.lights[this.index].enabled.set(this.enabled ? 1.0 : 0.0);
        shader.uniform.lights[this.index].color.set(this.color);
        shader.uniform.lights[this.index].direction.set(this.position);
        shader.uniform.lights[this.index].type.set(this.type);
        shader.uniform.lights[this.index].strength.set(this.point_strength);
    }
}

Light._index = 0; /// \todo Handle this in the scene (could have multiple gl context active at once)
Light.Type = Object.freeze({
    DIRECTIONAL: 0,
    POINT: 1,
});
