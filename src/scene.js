import {Color, Point2D, Point3D, Transform, TransformStack} from "./math.js";
import {Light, Texture} from "./drawable.js";
import {Ray} from "./collision.js";
import {downloader, Downloader} from "./downloader.js";


export class PerspectiveCamera
{
    constructor(fov, aspect_ratio, z_near, z_far)
    {
        this._fov = fov;
        this._aspect_ratio = aspect_ratio;
        this._z_near = z_near;
        this._z_far = z_far;
        this._projection = null;
        this.transform = new Transform();
    }

    get projection()
    {
        if ( this._projection === null )
        {
            this._projection = new Transform();
            this._projection.set_to_perspective(
                this.fov * Math.PI / 180,
                this.aspect_ratio,
                this.z_near,
                this.z_far
            );
        }
        return this._projection;
    }

    _getter(attrib)
    {
        return this["_"+attrib];
    }

    _setter(attrib, value)
    {
        if ( value !== this._getter(attrib) )
        {
            this._projection = null;
            return this["_"+attrib] = value;
        }
        return value;
    }

    get fov() { return this._getter("fov"); }
    set fov(v) { return this._setter("fov", v); }
    get aspect_ratio() { return this._getter("aspect_ratio"); }
    set aspect_ratio(v) { return this._setter("aspect_ratio", v); }
    get z_near() { return this._getter("z_near"); }
    set z_near(v) { return this._setter("z_near", v); }
    get z_far() { return this._getter("z_far"); }
    set z_far(v) { return this._setter("z_far", v); }

    full_matrix()
    {
        var m = this.projection.copy();
        m.multiply(this.transform);
        return m;
    }
}

export class GlScene
{
    constructor(gl, on_frame=null)
    {
        this.gl = gl;

        this.render_options = {
            bg_color: new Color(0, 0, 0),
            light_ambient_color: new Color(0, 0, 0),
        }
        this.camera = new PerspectiveCamera(45, 1, 0.1, 100),
        this.objects = [];
        this.frame_time = 0;
        this.on_frame = on_frame;
        this.textures = {};
        this.lights = [];
        this.lights.push(new Light(Light.Type.DIRECTIONAL, new Color(1, 1, 1), new Point3D(-1, 1, 1)));
        for ( var i = 1; i < 16; i++ )
            this.lights.push(new Light(Light.Type.POINT, new Color(1, 1, 1), new Point3D(0, 0, 0), 100, null, false));
        this.environment = new Texture(this.gl);
        this.transform = new TransformStack();
    }

    static linked(other, attrs=["camera", "lights", "environment", "transform"])
    {
        var scene = new this(other.gl);
        for ( var attr of attrs )
            scene[attr] = other[attr];
        return scene;
    }

    static from_canvas(canvas, gl_version="webgl2")
    {
        var gl = canvas.getContext(gl_version, {preserveDrawingBuffer: true});
        if ( !gl && gl_version != "webgl" )
        {
            console.warn("Could not initialize webgl2 context, falling back to webgl 1");
            gl = canvas.getContext('webgl', {preserveDrawingBuffer: true});
        }
        if ( !gl )
            throw "Unable to initialize WebGL. Your browser or machine may not support it.";
        gl.version = parseFloat(gl.getParameter(gl.VERSION).slice(6));

        return new this(gl);
    }

    load_texture(name, uri)
    {
        var texture = new Texture(this.gl);
        texture.from_uri(uri);
        this.textures[name] = texture;
        return texture;
    }

    load_video_texture(name, uri)
    {
        var texture = new Texture(this.gl);
        texture.from_video_uri(uri);
        this.textures[name] = texture;
        return texture;
    }

    draw_clear()
    {
        this.camera.aspect_ratio = this.gl.canvas.clientWidth / this.gl.canvas.clientHeight;
//         this.transform.transform = this.camera.transform.copy().transform;
    }

    render_start(renderer)
    {
        requestAnimationFrame((now => this.render_loop_frame(now, renderer)).bind(this));
    }

    render_loop_frame(now, renderer)
    {
        this.render_animation_frame(now, renderer);
        this.render_start(renderer);
    }

    render_animation_frame(now, renderer)
    {
        if ( downloader.loaded )
        {
            this.animation_frame_step(now);
            this.render_frame(renderer);
        }
    }

    animation_frame_step(now)
    {
        var delta_time = now - this.frame_time;
        this.frame_time = now;

        for ( var tex of Object.values(this.textures) )
            tex.update();

        if ( this.on_frame )
            this.on_frame(this, delta_time);
    }


    render_frame(renderer)
    {
        if ( downloader.loaded )
        {
            renderer.render(this);
        }
    }

    /**
     * \param canvas_p X Y coords as from the canvas object
     * \param clip_percentage [0, 1] to get point on the [near, far] clipping plane
     */
    reverse_project(canvas_p, clip_percentage)
    {
        var x = 2 * canvas_p.x / this.gl.canvas.clientWidth - 1;
        var y = -2 * canvas_p.y / this.gl.canvas.clientHeight + 1;
        var z = 2.0 * clip_percentage - 1.0;
        var pm = this.camera.full_matrix();
        pm.invert();
        var p = pm.lmultiply_v4([x,y,z,1]);
        var w = 1/p[3];
        return new Point3D(p[0]*w, p[1]*w, p[2]*w);
    }

    /**
     * \brief Projects a point in 3D space to canvas 2D coordinates
     */
    project(point)
    {
        var pm = this.camera.full_matrix();
        var p = pm.project(point);
        return new Point2D(
            (p.x + 1) / 2 * this.gl.canvas.clientWidth,
            (1 - p.y) / 2 * this.gl.canvas.clientHeight,
        );
    }

    reverse_project_ray(canvas_p)
    {
        return new Ray(
            this.reverse_project(canvas_p, 0),
            this.reverse_project(canvas_p, 1)
        );
    }

    _log_error()
    {
        var err = this.gl.getError();
        if ( !err )
            return false;
        var name = "Unknown";
        if ( err == this.gl.INVALID_ENUM )
            name = "INVALID_ENUM";
        else if ( err == this.gl.INVALID_VALUE )
            name = "INVALID_VALUE";
        else if ( err == this.gl.INVALID_OPERATION )
            name = "INVALID_OPERATION";
        else if ( err == this.gl.INVALID_FRAMEBUFFER_OPERATION )
            name = "INVALID_FRAMEBUFFER_OPERATION";
        else if ( err == this.gl.OUT_OF_MEMORY )
            name = "OUT_OF_MEMORY";
        else if ( err == this.gl.CONTEXT_LOST_WEBGL )
            name = "CONTEXT_LOST_WEBGL";

        console.warn(name, err);
        return true;
    }
}

export class PresentationScene extends GlScene
{
    constructor(gl, on_frame=null, ...args)
    {
        super(gl, on_frame, ...args);
        this.distance = 10;
        this.angles = new Point3D(0, 0, 0);

        this.dragdata = null;
        var element = gl.canvas;
        element.addEventListener("mousedown", this.on_mouse_start.bind(this));
        element.addEventListener("mousemove", this.on_mouse_move.bind(this));
        element.addEventListener("mouseup", this.on_mouse_stop.bind(this));
        element.addEventListener("mouseleave", this.on_mouse_stop.bind(this));
        element.addEventListener("wheel", this.on_scroll.bind(this));
        element.addEventListener("touchcancel", this.on_mouse_stop.bind(this));
        element.addEventListener("touchend", this.on_mouse_stop.bind(this));
        element.addEventListener("touchstart", this.on_touch_start.bind(this));
        element.addEventListener("touchmove", this.on_touch_move.bind(this));
    }

    draw_clear()
    {
        super.draw_clear();
        this.camera.transform.clear()
        this.camera.transform.translate(0, 0, -this.distance);
        this.camera.transform.rotate(this.angles.x * Math.PI / 180, 1, 0, 0);
        this.camera.transform.rotate(this.angles.z * Math.PI / 180, 0, 0, 1);
        this.camera.transform.rotate(this.angles.y * Math.PI / 180, 0, 1, 0);
    }

    on_mouse_start(ev)
    {
        this.dragdata = {
            start: new Point2D(ev.offsetX, ev.offsetY),
            start_angles: new Point3D(...this.angles.components),
            btn: ev.button
        };
    }

    on_mouse_stop(ev)
    {
        if ( ev.button === undefined || ev.button === null || (this.dragdata && ev.button == this.dragdata.btn) )
            this.dragdata = null;
    }

    on_mouse_move(ev)
    {
        if ( this.dragdata )
        {
            var point = new Point2D(ev.offsetX, ev.offsetY);
            point.subtract(this.dragdata.start);
            this.on_move(point);
        }
    }

    on_move(delta)
    {
        this.angles.y = this.dragdata.start_angles.y + delta.x / this.gl.canvas.clientWidth * 360;
        this.angles.x = this.dragdata.start_angles.x + delta.y / this.gl.canvas.clientHeight * 360;
    }

    on_scroll(ev)
    {
        if ( ev.deltaY > 0 )
            this.distance += 1;
        else if ( ev.deltaY < 0 )
            this.distance = Math.max(0, this.distance - 1);
    }

    on_touch_start(ev)
    {
        ev.preventDefault();
        if ( !this.dragdata )
        {
            this.dragdata = {
                touches: Array.from(ev.changedTouches).map(t => ({
                    s: new Point2D(t.clientX, t.clientY),
                    p: new Point2D(t.clientX, t.clientY),
                    id: t.identifier,
                })),
                start_angles: new Point3D(...this.angles.components),
                btn: ev.button,
                start_distance: this.distance,
            };
        }
        else
        {
            var found = new Set(this.dragdata.touches.map(x => x.id));
            for ( var t of ev.changedTouches )
            {
                if ( !found.has(t.identifier) )
                    this.dragdata.touches.push({
                        s: new Point2D(t.clientX, t.clientY),
                        p: new Point2D(t.clientX, t.clientY),
                        id: t.identifier,
                    });
            }
        }

        this.dragdata.size = (
            this.dragdata.touches
            .map(x => x.s)
            .reduce((a, b) => new Point2D(a.x - b.x, a.y - b.y))
            .length()
        );
    }

    on_touch_move(ev)
    {
        if ( this.dragdata )
        {
            ev.preventDefault();

            var touches = {};
            for ( var t of ev.changedTouches )
                touches[t.identifier] = t;

            for ( var t of this.dragdata.touches )
            {
                if ( touches[t.id] )
                    t.p = new Point2D(touches[t.id].clientX, touches[t.id].clientY);
            }

            if ( this.dragdata.touches.length == 1 )
            {
                var id = this.dragdata.touches[0].id;
                var t = ev.changedTouches[0];
                if ( t.identifier == id )
                {
                    var point = new Point2D(t.clientX, t.clientY);
                    point.subtract(this.dragdata.touches[0].s);
                    this.on_move(point);
                }
            }
            else if ( this.dragdata.touches.length == 2 )
            {
                var p = (
                    this.dragdata.touches
                    .map(x => x.p)
                    .reduce((a, b) => new Point2D(a.x - b.x, a.y - b.y))
                );
                var delta = p.length() - this.dragdata.size;
                this.distance = Math.max(0, this.dragdata.start_distance - delta / 17);
            }
        }
    }
}
