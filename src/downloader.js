export class Downloader
{
    constructor()
    {
        this.groups = {};
        this.total = 0;
        this.completed = 0;
        this.job_id = 0;
        this.sender_id = 0;
        this.on_update = null; ///< Passed (uri, Downloader.Status, downloader)
    }

    /**
     * \param on_load callable, will be passed (job)
     */
    download(uri, on_load, meta={}, group=null, mime="text/plain")
    {
        var job = this._setup_job(uri, on_load, meta, group);

        job.request = new XMLHttpRequest();
        job.request.addEventListener("load", function(){
            job.response = job.request.responseText;
            this._load_job(job);
        }.bind(this));
        if ( mime )
            job.request.overrideMimeType(mime);
        job.request.open("GET", job.uri);
        job.request.send();

        return job;
    }

    _setup_job(uri, on_load, meta, group)
    {
        var job = {
            id: this.job_id++,
            loaded: false,
            uri: uri,
            on_load: on_load,
            meta: meta,
            group: group,
            downloader: this,
            status: Downloader.Status.ADDED,
        };

        if ( group !== null )
        {
            if ( this.groups[group] === undefined )
                this.groups[group] = [];
            this.groups[group].push(job);
        }

        this.total += 1;
        this._on_update(job);

        return job;
    }

    download_image(uri, on_load, meta={}, group=null)
    {
        var job = this._setup_job(uri, on_load, meta, group);

        job.image = new Image();
        job.image.onload = function(){
            this._load_job(job);
        }.bind(this);
        job.image.src = job.uri;

        return job;
    }

    download_video(uri, on_load, meta={}, group=null)
    {
        var job = this._setup_job(uri, on_load, meta, group);

        job.video = document.createElement("video");

        job.video.addEventListener('canplaythrough', function()
        {
            this._load_job(job);
        }.bind(this), true);

        job.video.src = uri;

        return job;
    }

    _load_job(job)
    {
        job.status = Downloader.Status.LOAD_START;
        this._on_update(job);
        job.on_load(job);
        this.completed += 1;
        if ( job.status == Downloader.Status.LOAD_START )
            job.status = Downloader.Status.LOAD_END;
        this._on_update(job);
    }

    get progress()
    {
        if ( this.total == 0 )
            return 0;
        return this.completed / this.total;
    }

    get loaded()
    {
        return this.completed == this.total && this.total > 0;
    }

    clear_group(name)
    {
        delete this.groups[name];
    }

    group_is_completed(name, status=1)
    {
        if ( this.groups[name] === undefined )
            return false;

        return this.groups[name].reduce((a, v) => a && v.status >= status, true);
    }

    _on_update(job)
    {
        if ( this.on_update )
            this.on_update(job);
    }

    new_sender_id()
    {
        return this.sender_id++;
    }
}

Downloader.Status = Object.freeze({
    ADDED: 0,
    LOAD_START: 1,
    CUSTOM: 2,
    LOAD_END: 0xffff,
});

export const downloader = new Downloader();

