import {Material, Model, PointArray, Vertex, DrawMode} from "./drawable.js";
import {Point2D, Point3D} from "./math.js";


export function box(size_x=1, size_y=null, size_z=null, material=new Material())
{
    if ( size_y === null )
        size_y = size_x;

    if ( size_z === null )
        size_z = size_x;

    size_x /= 2;
    size_y /= 2;
    size_z /= 2;

    var cube = new Model([], material);
    cube.push_face([
        new Vertex(new Point3D(-size_x, -size_y, +size_z), new Point2D(0, 1)),
        new Vertex(new Point3D(+size_x, -size_y, +size_z), new Point2D(1, 1)),
        new Vertex(new Point3D(+size_x, +size_y, +size_z), new Point2D(1, 0)),
        new Vertex(new Point3D(-size_x, +size_y, +size_z), new Point2D(0, 0)),
    ], null, true);
    cube.push_face([
        new Vertex(new Point3D(-size_x, -size_y, -size_z), new Point2D(1, 1)),
        new Vertex(new Point3D(-size_x, +size_y, -size_z), new Point2D(1, 0)),
        new Vertex(new Point3D(+size_x, +size_y, -size_z), new Point2D(0, 0)),
        new Vertex(new Point3D(+size_x, -size_y, -size_z), new Point2D(0, 1)),
    ], null, true);
    cube.push_face([
        new Vertex(new Point3D(+size_x, -size_y, -size_z), new Point2D(1, 1)),
        new Vertex(new Point3D(+size_x, +size_y, -size_z), new Point2D(1, 0)),
        new Vertex(new Point3D(+size_x, +size_y, +size_z), new Point2D(0, 0)),
        new Vertex(new Point3D(+size_x, -size_y, +size_z), new Point2D(0, 1)),
    ], null, true);
    cube.push_face([
        new Vertex(new Point3D(-size_x, -size_y, -size_z), new Point2D(0, 1)),
        new Vertex(new Point3D(-size_x, -size_y, +size_z), new Point2D(1, 1)),
        new Vertex(new Point3D(-size_x, +size_y, +size_z), new Point2D(1, 0)),
        new Vertex(new Point3D(-size_x, +size_y, -size_z), new Point2D(0, 0)),
    ], null, true);
    cube.push_face([
        new Vertex(new Point3D(-size_x, -size_y, -size_z), new Point2D(0, 0)),
        new Vertex(new Point3D(+size_x, -size_y, -size_z), new Point2D(1, 0)),
        new Vertex(new Point3D(+size_x, -size_y, +size_z), new Point2D(1, 1)),
        new Vertex(new Point3D(-size_x, -size_y, +size_z), new Point2D(0, 1)),
    ], null, true);
    cube.push_face([
        new Vertex(new Point3D(-size_x, +size_y, -size_z), new Point2D(0, 0)),
        new Vertex(new Point3D(-size_x, +size_y, +size_z), new Point2D(0, 1)),
        new Vertex(new Point3D(+size_x, +size_y, +size_z), new Point2D(1, 1)),
        new Vertex(new Point3D(+size_x, +size_y, -size_z), new Point2D(1, 0)),
    ], null, true);
    return cube;
}

export function vertex_normals(drawable, material=Material.shadeless())
{
    var vertices = drawable.get_vertices();
    var out = new PointArray([], material, DrawMode.LINES);
    for ( var v of vertices )
    {
        out.push(new Vertex(v.point.clone()));
        var p = v.point.clone();
        p.add(v.normal);
        out.push(new Vertex(p));
    }
    return out;
}

export function triangle_normals(drawable, material=Material.shadeless())
{
    var vertices = drawable.get_vertices();
    var out = new PointArray([], material, DrawMode.LINES);
    for ( var i = 2; i < vertices.length; i += 3 )
    {
        var p1 = vertices[i-2].point;
        var p2 = vertices[i-1].point;
        var p3 = vertices[i].point;
        var mid = p1.clone();
        mid.add(p2);
        mid.add(p3);
        mid.divide(3);

        out.push(new Vertex(mid.clone()));
        mid.add(vertices[i].normal);
        out.push(new Vertex(mid));
    }
    return out;
}

export function sphere(radius=1, rings=8, sides=null, material=new Material())
{
    if ( sides === null )
        sides = rings;
    var sphere = new Model([], material);
    var side_angle = Math.PI * 2 / sides;
    var ring_angle = Math.PI / (rings - 1);
    var points = [];
    for ( var ring = 0; ring < rings; ring++ )
    {
        var ynorm = Math.cos(ring * ring_angle);
        var y = ynorm * radius;
        var ring_radius = Math.sin(Math.acos(ynorm)) * radius;
        var points_ring = [];
        points.push(points_ring);
        for ( var side = 0; side <= sides; side++ )
        {
            var angle = side_angle * side;
            var vert = new Point3D(Math.cos(angle) * ring_radius, y, Math.sin(angle) * ring_radius);
            var norm = vert.clone();
            norm.normalize();
            points_ring.push(new Vertex(vert, new Point2D(1-side/sides, -ynorm/2+0.5), norm));
        }
    }

    for ( var ring = 1; ring < points.length; ring++ )
    {
        var pr = points[ring-1];
        var npr = points[ring]
        for ( var side = 0; side < sides; side++ )
        {
            var nside = side + 1;
            sphere.push_face([pr[side], pr[nside], npr[nside], npr[side]]);
        }
    }

    return sphere;
}

export function frustum(radius=1, height=1, sides=4, top_radius=null, material=new Material())
{
    var out = new Model([], material);
    if ( top_radius === null )
        top_radius = radius;

    var face1 = [];
    var face2 = [];
    var side_angle = Math.PI * 2 / sides;
    for ( var i = 0; i <= sides; i++ )
    {
        var angle = side_angle * i;
        var vert1 = new Point3D(Math.cos(angle) * radius, 0, Math.sin(angle) * radius);
        var vert2 = new Point3D(Math.cos(angle) * top_radius, height, Math.sin(angle) * top_radius);
        var uv = new Point2D(0.5 + Math.sin(angle)/2, 0.5 - Math.cos(angle)/2);
        face1.push(new Vertex(vert1, uv, new Point3D(0, -1, 0)));
        face2.push(new Vertex(vert2, uv.clone(), new Point3D(0, 1, 0)));
    }

    out.push_face(Array.from(face1));
    for ( var i = 0; i < sides; i++ )
    {
        out.push_face([
            new Vertex(face2[i].point.clone(), new Point2D(1 - i / sides, 0)),
            new Vertex(face2[i+1].point.clone(), new Point2D(1 - (i + 1) / sides, 0)),
            new Vertex(face1[i+1].point.clone(), new Point2D(1 - (i + 1) / sides, 1)),
            new Vertex(face1[i].point.clone(), new Point2D(1 - i / sides, 1)),
        ], null, true);

    }
    out.push_face(face2);
    return out;
}
